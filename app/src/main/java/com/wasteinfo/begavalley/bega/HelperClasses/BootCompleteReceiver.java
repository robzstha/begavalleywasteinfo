package com.wasteinfo.begavalley.bega.HelperClasses;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.wasteinfo.begavalley.bega.AlarmServices.AlarmNotificationReceiver;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by core I5 on 8/1/2017.
 */

public class BootCompleteReceiver extends BroadcastReceiver {

    private final String TAG = BootCompleteReceiver.class.getSimpleName();
    private SharedPref pref;
    private SharedPreferences datePref;

    @Override
    public void onReceive(Context context, Intent intent) {
        pref = new SharedPref(context);
        datePref = context.getSharedPreferences("DATE_PREFS_FILE", Context.MODE_PRIVATE);
        int itemID = pref.getIntValues("item_id");
        if (itemID != 0) {
            setAlarms(context);
        }

    }

    private void setAlarms(Context context) {
        try {
            ArrayList<Calendar> calendars = (ArrayList<Calendar>) ObjectSerializer.deserialize(datePref
                    .getString(
                            "Calendar_Date_List",
                            ObjectSerializer
                                    .serialize(new ArrayList<Calendar>())));
            if (calendars != null && !calendars.isEmpty()) {
                Calendar date = calendars.get(0);
                Calendar cal = Calendar.getInstance();
                while ((date.get(Calendar.MONTH) <= cal.get(Calendar.MONTH)) && date.get(Calendar.DAY_OF_MONTH) <= cal.get(Calendar.DAY_OF_MONTH)) {
                    date.add(Calendar.DAY_OF_MONTH, 7);
                }

                if (date.get(Calendar.DAY_OF_MONTH) - 1 == cal.get(Calendar.DAY_OF_MONTH)) {
                    if (cal.get(Calendar.HOUR_OF_DAY) >= 18 && cal.get(Calendar.MINUTE) > 0
                            && cal.get(Calendar.SECOND) > 0) {
                        date.add(Calendar.DAY_OF_MONTH, 7);
                    }
                }

                if (date.get(Calendar.MONTH) - 1 == cal.get(Calendar.MONTH) && (date.get(Calendar.DAY_OF_MONTH) == 1) && (cal.getActualMaximum(Calendar.DATE) == cal.get(Calendar.DAY_OF_MONTH))) {
                    if (cal.get(Calendar.HOUR_OF_DAY) >= 18 && cal.get(Calendar.MINUTE) > 0) {
                        date.add(Calendar.DAY_OF_MONTH, 7);
                    }
                }
                int all_collections = pref.getIntValues("check_flag_all");
                int next_collection = pref.getIntValues("check_flag_next");

                if (all_collections == 1) {
                    Intent myIntent = new Intent(context, AlarmNotificationReceiver.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, myIntent, 0);

                    // Get the AlarmManager service
                    AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);

                    date.add(Calendar.DATE, -1);
                    date.set(Calendar.HOUR_OF_DAY, 18);
                    date.set(Calendar.MINUTE, 0);
                    date.set(Calendar.SECOND, 0);

                    am.setRepeating(AlarmManager.RTC_WAKEUP, date.getTimeInMillis(), 604800000, pendingIntent);
                } else if (next_collection == 1) {
                    Intent myIntent = new Intent(context, AlarmNotificationReceiver.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, myIntent, 0);

                    // Get the AlarmManager service
                    AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);

                    date.add(Calendar.DATE, -1);
                    date.set(Calendar.HOUR_OF_DAY, 18);
                    date.set(Calendar.MINUTE, 0);
                    date.set(Calendar.SECOND, 0);

                    am.set(AlarmManager.RTC, date.getTimeInMillis(), pendingIntent);
                }

            } else {
                Log.d(TAG, "No dates found!!!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
