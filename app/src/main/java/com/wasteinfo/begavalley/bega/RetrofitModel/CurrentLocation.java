package com.wasteinfo.begavalley.bega.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Test on 11/4/2016.
 */

public class CurrentLocation {

    @SerializedName("formatted_address")
    @Expose
    public String formattedAddress;
    @SerializedName("place_id")
    @Expose
    public String placeId;
    @SerializedName("types")
    @Expose
    public List<String> types = new ArrayList<String>();
    @SerializedName("postcode_localities")
    @Expose
    public List<String> postcodeLocalities = new ArrayList<String>();
}
