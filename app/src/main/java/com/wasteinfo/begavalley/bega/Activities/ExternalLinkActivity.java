package com.wasteinfo.begavalley.bega.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.R;

public class ExternalLinkActivity extends ActionBarActivity implements View.OnClickListener{

private static final String LOGTAG = "WasteInfoVal";
	
	
    Library library;
	
	private WebView web;
	
	private Context context;
	
		
	
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar
//		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
				
		//Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
				
				
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			
		setContentView(R.layout.activity_external_link);
		
		final ProgressDialog pd = ProgressDialog.show(this, "", "Loading...", true);
		this.context = this;
		this.library=new Library(this.context);
		this.web = (WebView)this.findViewById(R.id.waste_item_webView);
		//this.web.setWebViewClient(new WebViewClient());
		this.web.getSettings().setAppCacheMaxSize( 10 * 1024 * 1024 );
		this.web.getSettings().setAppCachePath( getApplicationContext().getCacheDir().getAbsolutePath()+ "/cache" );
		this.web.getSettings().setAllowFileAccess( true );
		this.web.getSettings().setAppCacheEnabled( true );
		this.web.getSettings().setJavaScriptEnabled( true );
		this.web.getSettings().setDomStorageEnabled( true );
		this.web.getSettings().setCacheMode( WebSettings.LOAD_DEFAULT );
		this.web.setWebChromeClient(new WebChromeClient()); 
		this.web.setWebViewClient(new WebViewClient());
        
		
		
        

		
		Bundle bundle = getIntent().getExtras();
		String url = bundle.getString("external_url");
		String temp = bundle.getString("temp");
		
		this.web.setWebViewClient(new WebViewClient() {
		    @Override
		    public void onPageFinished(WebView view, String url) {
		        pd.dismiss();
		    }
		});
		
		try {
			this.web.loadUrl(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.web.getSettings().setBuiltInZoomControls(true);
		
		
		
		
		
		

		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(event.getAction() == KeyEvent.ACTION_DOWN){
            switch(keyCode)
            {
            case KeyEvent.KEYCODE_BACK:
                if(this.web.canGoBack()){
                	this.web.goBack();
                }else{
                    finish();
                }
                return true;
            }

        }
        return super.onKeyDown(keyCode, event);
	}

	
	@Override
	public void onClick(View v) {
		

	}

	

		
}
