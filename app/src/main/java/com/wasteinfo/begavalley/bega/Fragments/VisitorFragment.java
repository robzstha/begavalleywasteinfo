package com.wasteinfo.begavalley.bega.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.Activities.AddressActivity;
import com.wasteinfo.begavalley.bega.OnBackPressedListener;
import com.wasteinfo.begavalley.bega.RetrofitModel.Page;
import com.wasteinfo.begavalley.bega.db.PageDataSource;

import java.util.ArrayList;

public class VisitorFragment extends Fragment implements OnBackPressedListener {
    private WebView web;
    private PageDataSource pageDataSource;
    int page_id1 = 163;
    Page page;
    private String head;
    private String footer;
    private String baseUrl;
    private ArrayList<String> contentArray;
    private ArrayList<String> titleArray;
    String finalHtmlString;
    private AddressActivity addressActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.addressActivity = (AddressActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootview = inflater.inflate(R.layout.fragment_visitor, container, false);
        this.contentArray = new ArrayList<String>();
        this.titleArray = new ArrayList<String>();
        addressActivity.tv_title.setText("Visitor");

        ((AddressActivity) getActivity()).setOnBackPressedListener(this);

        this.web = (WebView) rootview.findViewById(R.id.page_content);
        this.web.setWebViewClient(new WebViewClient());
        this.web.setScrollbarFadingEnabled(false);
        this.web.getSettings().setBuiltInZoomControls(true);
        this.web.getSettings().setDisplayZoomControls(false);
        //this.web.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 4.1.1; HTC One X Build/JRO03C) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.58 Mobile Safari/537.31");
        this.web.getSettings().setJavaScriptEnabled(true);
        pageDataSource = new PageDataSource(getActivity());
        this.pageDataSource.open();
        this.page = this.pageDataSource.getPage(page_id1);
        this.pageDataSource.close();
        head = "<html><style>@font-face {font-family: 'exo-light'; src: url('fonts/Exo_Light.TTF'); }</style><body>";
        footer = "</body></html>";

        if (page != null && !page.getPageContent().isEmpty()) {
            finalHtmlString = head + this.page.getPageContent() + footer;
        } else {
            finalHtmlString = "";
        }
        if (!finalHtmlString.isEmpty()) {
            try {
                this.web.loadData(finalHtmlString, "text/html", "UTF-8");
//            this.pageContent.loadData(head + page.getPageContent() + footer, "text/html", "UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return rootview;
    }

    @Override
    public void doBack() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Are you sure you want to exit?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                addressActivity.finishAffinity();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
//if (finalHtmlString.startsWith("tel:")) {
//        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(finalHtmlString));
//        startActivity(intent);
//
//        }