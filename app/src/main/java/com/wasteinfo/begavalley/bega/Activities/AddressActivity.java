package com.wasteinfo.begavalley.bega.Activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.bega.AlarmServices.AlarmNotificationReceiver;
import com.wasteinfo.begavalley.bega.Fragments.AddressFragment;
import com.wasteinfo.begavalley.bega.Fragments.CalendarFragment;
import com.wasteinfo.begavalley.bega.Fragments.Material;
import com.wasteinfo.begavalley.bega.Fragments.ReportProblem;
import com.wasteinfo.begavalley.bega.Fragments.VisitorFragment;
import com.wasteinfo.begavalley.bega.Fragments.WasteMaterials;
import com.wasteinfo.begavalley.bega.HelperClasses.AutocompleteArrayAdapter;
import com.wasteinfo.begavalley.bega.HelperClasses.KeyboardHelper;
import com.wasteinfo.begavalley.bega.HelperClasses.ObjectSerializer;
import com.wasteinfo.begavalley.bega.HelperClasses.Opener;
import com.wasteinfo.begavalley.bega.OnBackPressedListener;
import com.wasteinfo.begavalley.bega.RetrofitModel.StreetResponse;
import com.wasteinfo.begavalley.bega.RetrofitModel.Suburbs;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.WebServiceTasks.GetLocation;
import com.wasteinfo.begavalley.bega.WebServiceTasks.GetSuburbs;
import com.wasteinfo.begavalley.bega.db.LocationDataSource;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class AddressActivity extends AppCompatActivity implements AHBottomNavigation.OnTabSelectedListener,
        NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener,
        View.OnFocusChangeListener, GetSuburbs.onGetSuburb, GetLocation.OnGetLocation {
    protected Opener opener;
    private static final String LOGTAG = "WasteInfo";
    public static AHBottomNavigation bottomNavigation;
    public TextView tv_title;
    List<StreetResponse.Streets> dataStreetList;
    ArrayList<String> streetNameList;
    HashMap hashMapStreet;
    ArrayList<Integer> streetType;
    ArrayList<StreetResponse.Streets> addressSuggestion;
    private RadioButton radio_resident;
    private RadioButton radio_guest;
    private LinearLayout linearLayout;
    private Button btn_proceed;
    private AutoCompleteTextView base_suburb;
    private AutoCompleteTextView base_street;
    private EditText et_street_no;
    String push_notification_message;
    private Button btn_set_alarm;
    private Button btn_organic, btn_recycling, btn_garbage;
    private CheckBox next_collection;
    private CheckBox all_collection;
    private TextView all;
    private TextView next;
    private TextView tvRuralBase;
    private TextView tvRuralPage;
    String organic_date, garbage_date;

    private long item_id;
    private int bin_type_id;
    private String bin_colors;
    private ArrayList<Calendar> date_list;
    private GetLocation getLocation;
    public DrawerLayout drawer;
    AlarmNotificationReceiver alm;


    private ArrayList<Suburbs.Data> suburb_list;
    private ArrayList<String> addressAuto;
    ArrayList<String> sub_list;
    private AutocompleteArrayAdapter adapter;
    private SharedPreferences preferences;
    String suburbname;
    private Library library;
    public ActionBarDrawerToggle toggle;
    private SharedPref pref;
    private String checkRadio = "2";
    private LocationDataSource datasource;
    private KeyboardHelper kb_helper;
    private String recycling_date;
    private Calendar garbage_cal;
    private Calendar recycle_cal;
    private Calendar organic_cal;
    String surUrbList;
    int selectedStreet;
    //    private KeyboardHelper kb_helper;
    Bundle bundle;
    ArrayList<Suburbs.Data> subUrbList;
    public ArrayList<String> Suburbname;
    String subUrbNames;
    public List<Suburbs.Data> subUrbData;
    ArrayList<String> SubUrbNamesList;
    ArrayList<Integer> SubUrbTypeList;
    private GetSuburbs suburbs;
    private SharedPreferences datePrefs;
    int selected;

    public static List<Integer> fr_id = new ArrayList<>();
    private OnBackPressedListener onBackPressedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        Log.d("AddressActivity", "onCreate: ");

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        kb_helper = new KeyboardHelper();
//        kb_helper.setupUI(findViewById(R.id.sliding_layout), this);
        opener = new Opener(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.sliding_menu_button);
        this.datePrefs = this.getSharedPreferences("DATE_PREFS_FILE",
                Context.MODE_PRIVATE);

        this.library = new Library(this);
        pref = new SharedPref(this);
        alm = new AlarmNotificationReceiver();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            surUrbList = bundle.getString("Suburbs");
        }
        subUrbData = new ArrayList<Suburbs.Data>();
        Type type = new TypeToken<List<Suburbs.Data>>() {
        }.getType();
        List<Suburbs.Data> subUrbs = new Gson().fromJson(surUrbList, type);
        if (subUrbs != null && subUrbs.size() != 0) {
            subUrbData.addAll(subUrbs);
        }
        this.getLocation = new GetLocation(this, this);

        drawer = (DrawerLayout) findViewById(R.id.drayer_id);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);


        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //change the menu icon
        toggle.setDrawerIndicatorEnabled(false);


        drawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                base_suburb.getText().clear();
                base_street.getText().clear();
                et_street_no.getText().clear();
                base_street.setHint(getResources().getText(R.string.no_address));
                base_street.setHintTextColor(getResources().getColor(R.color.white));
                et_street_no.setHint(getResources().getText(R.string.no_address));
                et_street_no.setHintTextColor(getResources().getColor(R.color.white));
                ButtonHide();
                collectionCheckBoxCheck();
            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {


            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerview = navigationView.getHeaderView(0);

//        parent.setContentInsetsAbsolute(0, 0);

        tv_title = (TextView) toolbar.findViewById(R.id.title_text);
        btn_proceed = (Button) headerview.findViewById(R.id.baseproceed_settings);
        tvRuralBase = (TextView) headerview.findViewById(R.id.bd_tv_rural);
        base_suburb = (AutoCompleteTextView) headerview.findViewById(R.id.suburb_settings);
        base_street = (AutoCompleteTextView) headerview.findViewById(R.id.address_settings);
        et_street_no = (EditText) headerview.findViewById(R.id.bd_et_street_no);
        btn_set_alarm = (Button) headerview.findViewById(R.id.reminder_btn);
        btn_garbage = (Button) headerview.findViewById(R.id.menu_btn_garbage);
        btn_recycling = (Button) headerview.findViewById(R.id.menu_btn_recycling);
        btn_organic = (Button) headerview.findViewById(R.id.menu_btn_organics);
        this.tvRuralPage = (TextView) headerview.findViewById(R.id.tv_rural);

        all = (TextView) headerview.findViewById(R.id.reminder_label_all_collections);
        next = (TextView) headerview.findViewById(R.id.reminder_label_next_collection);

        next_collection = (CheckBox) headerview.findViewById(R.id.checkbox_next_collection);
        all_collection = (CheckBox) headerview.findViewById(R.id.reminder_checkbox_all_collections);


        SubUrbNamesList = new ArrayList<String>();
        SubUrbTypeList = new ArrayList<Integer>();
        final HashMap<String, Integer> hashMap = new HashMap();
        for (int i = 0; i < subUrbData.size(); i++) {
            SubUrbNamesList.add(subUrbData.get(i).suburb);
            SubUrbTypeList.add(subUrbData.get(i).type);

        }

        for (int i = 0; i < SubUrbNamesList.size(); i++) {
            if (!SubUrbNamesList.isEmpty()) {
                String address = SubUrbNamesList.get(i).toString();
                if (!address.trim().isEmpty()) {
                    String output = address.substring(0, 1).toUpperCase().trim() + address.substring(1).toLowerCase().trim();
                    output = toTitleCase(output);
                    SubUrbNamesList.set(i, output);
                    //SubUrbNamesList.add(i,output);
                }
            }
        }

        for (int i = 0; i < SubUrbNamesList.size(); i++) {
            if (!SubUrbNamesList.isEmpty() && !SubUrbTypeList.isEmpty()) {
                hashMap.put(SubUrbNamesList.get(i), SubUrbTypeList.get(i));
            }
        }
        this.adapter = new AutocompleteArrayAdapter(this, SubUrbNamesList);
        this.base_suburb.setAdapter(adapter);
        this.base_suburb.setThreshold(1);
        kb_helper.setupUI(headerview.findViewById(R.id.Rl_base), this);
        kb_helper.setupUI(headerview.findViewById(R.id.Ll_base), this);

        checkHandler();


        btn_proceed.setOnClickListener(this);
        btn_set_alarm.setOnClickListener(this);


        this.base_suburb.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pref.setKeyValues("selected_suburb", parent.getItemAtPosition(position).toString());

                selected = hashMap.get(parent.getItemAtPosition(position));
                if (selected == 1) {
                    base_street.setEnabled(true);
                    et_street_no.setEnabled(false);
                    base_street.setHint(getResources().getText(R.string.address));
                    base_street.setHintTextColor(getResources().getColor(R.color.grey_dark));
                } else {
                    base_street.setHint(getResources().getText(R.string.no_address));
                    base_street.setHintTextColor(getResources().getColor(R.color.white));
                    base_street.setEnabled(false);
                }
            }
        });
        tvRuralBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VisitorFragment visitorFragment = new VisitorFragment();
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                        .replace(R.id.contentID, visitorFragment).commit();
                closeDrawer();

            }
        });

        this.base_street.setOnFocusChangeListener(this);
        tv_title.setTypeface(this.library.getDefaultFont("regular"));
        this.btn_proceed.setTypeface(this.library.getDefaultFont("regular"));
        this.base_street.setTypeface(this.library.getDefaultFont("regular"));
        this.base_suburb.setTypeface(this.library.getDefaultFont("regular"));
        this.et_street_no.setTypeface(this.library.getDefaultFont("regular"));
        this.btn_set_alarm.setTypeface(this.library.getDefaultFont("regular"));
        this.all.setTypeface(this.library.getDefaultFont("regular"));
        this.next.setTypeface(this.library.getDefaultFont("regular"));

        linearLayout = (LinearLayout) findViewById(R.id.contentID);

        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation_id);
        bottomNavigation.setOnTabSelectedListener(this);
        createNavBarItems();
    }


    public void ButtonHide() {
        this.item_id = pref.getIntValues("item_id");
        datasource = new LocationDataSource(this);
        datasource.open();
        String extractedOrganicDate = datasource.getOrganicDateByItemId(item_id);

        if (extractedOrganicDate == null || extractedOrganicDate.isEmpty()) {
            this.btn_organic.setVisibility(View.GONE);
        } else {
            this.btn_organic.setVisibility(View.VISIBLE);
        }

        String extractedRecyclingDate = datasource.getRecyclingDateByItemId(item_id);
        if (extractedRecyclingDate == null || extractedRecyclingDate.isEmpty()) {
            this.btn_recycling.setVisibility(View.GONE);

        } else {
            this.btn_recycling.setVisibility(View.VISIBLE);
        }

        String extractedGarbageDate = datasource.getGarbageDateByItemId(item_id);
        if (extractedGarbageDate == null || extractedGarbageDate.isEmpty()) {
            this.btn_garbage.setVisibility(View.GONE);
        } else {
            this.btn_garbage.setVisibility(View.VISIBLE);
        }
    }


    private void collectionCheckBoxCheck() {
        if (pref != null) {
            int check_flag_all = pref.getIntValues("check_flag_all");
            int check_flag_next = pref.getIntValues("check_flag_next");

            if (check_flag_all == 1) {
                this.all_collection.setChecked(true);
                this.next_collection.setChecked(true);
            } else {
                this.all_collection.setChecked(false);
                if (check_flag_next == 1) {
                    this.next_collection.setChecked(true);
                } else {
                    this.next_collection.setChecked(false);
                }
            }
        }
    }

    private void checkHandler() {
        collectionCheckBoxCheck();
        this.all_collection
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        if (all_collection.isChecked()) {
                            Log.d(LOGTAG, "All collection checked!");
                            next_collection.setChecked(true);
                        } else {
                            Log.d(LOGTAG, "All collection unchecked!");
                            next_collection.setChecked(false);
                        }
                    }
                });
        this.next_collection.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (!next_collection.isChecked()) {
                    all_collection.setChecked(false);
                }

            }
        });
    }

    public void createNavBarItems() {
        //Create Items
        AHBottomNavigationItem calendar = new AHBottomNavigationItem("Calendar", R.drawable.a_default_ico_menu);
        AHBottomNavigationItem materials = new AHBottomNavigationItem("Materials", R.drawable.b_default_ico_menu);
        AHBottomNavigationItem reportProblem = new AHBottomNavigationItem("Other Services", R.drawable.c_default_ico_menu);
        AHBottomNavigationItem wasteMaterial = new AHBottomNavigationItem("More information", R.drawable.d_default_ico_menu);

        // Disable the translation inside the CoordinatorLayout
        bottomNavigation.setBehaviorTranslationEnabled(true);
        //Adding Items
        bottomNavigation.addItem(calendar);
        bottomNavigation.addItem(materials);
        bottomNavigation.addItem(reportProblem);
        bottomNavigation.addItem(wasteMaterial);

        //Setting Properties
        bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.orange));
        bottomNavigation.setInactiveColor(getResources().getColor(R.color.black));
        bottomNavigation.setForceTitlesDisplay(true);
        bottomNavigation.setAccentColor(getResources().getColor(R.color.white));
        bottomNavigation.setBehaviorTranslationEnabled(false);
        bottomNavigation.setCurrentItem(0);
    }

    @Override
    public void onTabSelected(int position, boolean wasSelected) {
        push_notification_message = pref.getStringValues("notification_message");
        long locationId = pref.getIntValues("item_id");
        if (position == 0) {
            fr_id.clear();
            fr_id.add(0);
            if (locationId != 0) {
                CalendarFragment calendarFragment = new CalendarFragment();
                Bundle args = new Bundle();
                args.putInt("section_id", 3);
                args.putString("notofication_message", push_notification_message);
                calendarFragment.setArguments(args);
                getSupportFragmentManager().beginTransaction().
                        setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                        .replace(R.id.contentID, calendarFragment).addToBackStack(null).commit();

            } else {
                AddressFragment addressFragment = new AddressFragment();
                getSupportFragmentManager().beginTransaction().
                        setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                        .replace(R.id.contentID, addressFragment).addToBackStack(null).commit();
            }

        } else if (position == 1) {
            if (!fr_id.contains(1)) {
                fr_id.add(1);
            } else {
                fr_id.remove((Integer) 1);
                fr_id.add(1);
            }
            Material materialFragment = new Material();
            Bundle args = new Bundle();
            args.putInt("section_id", 2);
            materialFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                    .replace(R.id.contentID, materialFragment).addToBackStack(null).commit();

        } else if (position == 2) {
            if (!fr_id.contains(2)) {
                fr_id.add(2);
            } else {
                fr_id.remove((Integer) 2);
                fr_id.add(2);
            }
            ReportProblem reportFragment = new ReportProblem();
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                    .replace(R.id.contentID, reportFragment).addToBackStack(null).commit();
        } else if (position == 3) {
            if (!fr_id.contains(3)) {
                fr_id.add(3);
            } else {
                fr_id.remove((Integer) 3);
                fr_id.add(3);
            }
            WasteMaterials wasteFragment = new WasteMaterials();
            Bundle args = new Bundle();
            args.putInt("section_id", 1);
            wasteFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                    .replace(R.id.contentID, wasteFragment).addToBackStack(null).commit();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListener != null) {
            onBackPressedListener.doBack();
        } else {
            super.onBackPressed();
        }
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setMessage("Are you sure you want to exit?");
//        builder.setCancelable(false);
//        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                finishAffinity();
//            }
//        });
//        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//        AlertDialog alert = builder.create();
//        alert.show();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == btn_proceed.getId()) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

            String suburb_text = base_suburb.getText().toString().trim();
            String address_text = base_street.getText().toString().trim();
            String address_no_text = et_street_no.getText().toString().trim();

            if (suburb_text.isEmpty()) {
                Toast toast = Toast.makeText(this,
                        "Please search for your address", Toast.LENGTH_LONG);
                toast.show();
            } else if (address_text.isEmpty() && selected == 1) {
                Toast toast = Toast.makeText(this,
                        "Please enter your street name", Toast.LENGTH_LONG);
                toast.show();
            } else if (selectedStreet == 1 && address_no_text.isEmpty()) {

                Toast toast = Toast.makeText(this,
                        "Please enter your street number", Toast.LENGTH_LONG);
                toast.show();

            } else {
                pref.setKeyValues("user_suburb", suburb_text);
                pref.setKeyValues("user_address", address_text);
                pref.setKeyValues("user_street_number", address_text);

                base_suburb.getText().clear();
                base_street.getText().clear();
                et_street_no.getText().clear();
                if (library.isConnectingToInternet()) {
                    getLocation.getLocationMethod(suburb_text, address_text, address_no_text);
                } else {
                    Toast.makeText(AddressActivity.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
                }
            }
        }

        if (v.getId() == btn_set_alarm.getId())

        {
            Log.d("test", "set button clicked!");
            this.item_id = pref.getIntValues("item_id");
            this.bin_type_id = pref.getIntValues("bin_type_id");
            this.bin_colors = pref.getStringValues("bin_colors");

            if (item_id != 0) {

                // retrieve the attachment dates from the shared preferences
                try {

                    date_list = (ArrayList<Calendar>) ObjectSerializer.deserialize(datePrefs
                            .getString(
                                    "Calendar_Date_List",
                                    ObjectSerializer
                                            .serialize(new ArrayList<Calendar>())));

                    for (int i = 0; i < date_list.size(); i++) {

                        Log.d(LOGTAG, "Result==" + date_list.get(i));
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

                Calendar date = date_list.get(0);
                Calendar cal = Calendar.getInstance();

                while ((date.get(Calendar.MONTH) <= cal.get(Calendar.MONTH)) && date.get(Calendar.DAY_OF_MONTH) <= cal.get(Calendar.DAY_OF_MONTH)) {
                    date.add(Calendar.DAY_OF_MONTH, 7);
                }

                if (date.get(Calendar.DAY_OF_MONTH) - 1 == cal.get(Calendar.DAY_OF_MONTH)) {
                    if (cal.get(Calendar.HOUR_OF_DAY) >= 18 && cal.get(Calendar.MINUTE) > 0
                            && cal.get(Calendar.SECOND) > 0) {
                        date.add(Calendar.DAY_OF_MONTH, 7);
                    }
                }

                if (date.get(Calendar.MONTH) - 1 == cal.get(Calendar.MONTH) && (date.get(Calendar.DAY_OF_MONTH) == 1) && (cal.getActualMaximum(Calendar.DATE) == cal.get(Calendar.DAY_OF_MONTH))) {
                    System.out.print("prajit minutes" + cal.get(Calendar.MINUTE));
                    if (cal.get(Calendar.HOUR_OF_DAY) >= 18 && cal.get(Calendar.MINUTE) > 0) {
                        date.add(Calendar.DAY_OF_MONTH, 7);
                    }
                }

                if (all_collection.isChecked()) {
                    if (!date_list.isEmpty()) {

                        Intent myIntent = new Intent(this,
                                AlarmNotificationReceiver.class);
                        PendingIntent pendingIntent = PendingIntent
                                .getBroadcast(this, 0, myIntent,
                                        0);

                        // Get the AlarmManager service
                        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);

                        date.add(Calendar.DATE, -1);
                        date.set(Calendar.HOUR_OF_DAY, 18);
                        date.set(Calendar.MINUTE, 0);
                        date.set(Calendar.SECOND, 0);

                        am.setRepeating(AlarmManager.RTC_WAKEUP,
                                date.getTimeInMillis(), 604800000,
                                pendingIntent);
                        // set repeating every 1
                        // week

                        if (pref != null) {
                            pref.setKeyValues("check_flag_all", 1);
                        }
                    } else {
                        all_collection.setChecked(false);
                        next_collection.setChecked(false);
                        showmessage("There are no collection dates on this month.");
                    }

                    next_collection.setChecked(true);
                    if (pref != null) {
                        pref.setKeyValues("check_flag_next", 1);
                    }

                }

                if (!all_collection.isChecked()) {

                    if (next_collection.isChecked()) {
                        Log.d(LOGTAG, "Inside next_collection.isChecked()");
                        if (!date_list.isEmpty()) {
//                                Calendar date = date_list.get(0);

                            Intent myIntent = new Intent(this,
                                    AlarmNotificationReceiver.class);
                            PendingIntent pendingIntent = PendingIntent
                                    .getBroadcast(this, 0,
                                            myIntent, 0);

                            // Get the AlarmManager service
                            AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);

                            date.add(Calendar.DATE, -1);
                            date.set(Calendar.HOUR_OF_DAY, 18);
                            date.set(Calendar.MINUTE, 0);
                            date.set(Calendar.SECOND, 0);

                            am.set(AlarmManager.RTC,
                                    date.getTimeInMillis(), pendingIntent);

                            if (pref != null) {
                                pref.setKeyValues("check_flag_next", 1);
                            }
                        } else {
                            showNoCollectionMessage();
                        }
                    }
                }

                if (pref != null) {
                    if (pref.getIntValues("check_flag_all") == 1) {
                        if (!all_collection.isChecked() && !next_collection.isChecked()) {
                            pref.setKeyValues("check_flag_all", 0);
                            pref.setKeyValues("check_flag_next", 0);
                            cancelAlarm();
                            Toast.makeText(this, "Reminder cancelled", Toast.LENGTH_SHORT).show();
                        } else if (!all_collection.isChecked() && next_collection.isChecked()) {
                            pref.setKeyValues("check_flag_all", 0);
                        }
                    } else if (pref.getIntValues("check_flag_next") == 1) {
                        if (!next_collection.isChecked()) {
                            pref.setKeyValues("check_flag_next", 0);
                            cancelAlarm();
                            Toast.makeText(this, "Reminder cancelled", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (!all_collection.isChecked() && !next_collection.isChecked()) {
                            Toast.makeText(this, "Please set a time to be reminded the day before your collection", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    if (!all_collection.isChecked() && !next_collection.isChecked()) {
                        Toast.makeText(this, "Please set a time to be reminded the day before your collection", Toast.LENGTH_SHORT).show();
                    }
                }

                if (all_collection.isChecked()
                        || next_collection.isChecked()) {
                    if (!date_list.isEmpty()) {
                        displayMessage();
                    }
                }

            } else {
                showMessage();
            }

        }

    }

    private void cancelAlarm() {
        Intent myIntent = new Intent(this,
                AlarmNotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent
                .getBroadcast(this, 0, myIntent,
                        0);
        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        try {
            am.cancel(pendingIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        String suburb_txt = pref.getStringValues("selected_suburb");
        if (!this.base_street.hasFocus()) {
            base_street.setHint(getResources().getText(R.string.no_address));
            base_street.setHintTextColor(getResources().getColor(R.color.white));
        }
        if (!this.et_street_no.hasFocus()) {
            et_street_no.setHint(getResources().getText(R.string.no_address));
            et_street_no.setHintTextColor(getResources().getColor(R.color.white));
        }
        if (et_street_no.hasFocus()) {
            if (selectedStreet == 1) {
                et_street_no.setEnabled(true);
                et_street_no.setHint(getResources().getText(R.string.street_no));
                et_street_no.setHintTextColor(getResources().getColor(R.color.grey_dark));
            }
        }
        if (this.base_street.hasFocus()) {
            if (selected == 1) {
                base_street.setEnabled(true);
                et_street_no.setEnabled(false);
                base_street.setHint(getResources().getText(R.string.address));
                base_street.setHintTextColor(getResources().getColor(R.color.grey_dark));
            }
            if (this.library.isConnectingToInternet()) {
                String suburb_editTxt = this.base_suburb.getText().toString();
                if (!suburb_editTxt.isEmpty()) {
                    suburb_editTxt = toTitleCase(suburb_editTxt);
                }
                if (this.SubUrbNamesList.contains(suburb_editTxt)) {
                    if (this.base_suburb.getText().length() != 0 && suburb_editTxt.equals(suburb_txt)) {
                        if (this.library.isConnectingToInternet()) {
                            suburbs = new GetSuburbs(this, this);
                            suburbs.getAutoStreets(suburb_editTxt);
                        } else {
                            Toast toast = Toast.makeText(this, "No internet connection!", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                        pref.setKeyValues("selected_suburb", suburb_editTxt);
                        this.base_street.setText("");
                    }
                } else {
                    adapter = new AutocompleteArrayAdapter(this, null);
                    this.base_street.setAdapter(adapter);
                    pref.setKeyValues("selected_suburb", suburb_editTxt);
                }
            } else {
                Toast toast = Toast.makeText(this, "No internet connection!", Toast.LENGTH_SHORT);
                toast.show();
            }
        }


    }

    public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals("Kb")) {
                sb.append("KB").append(" ");
            } else {
                sb.append(Character.toUpperCase(arr[i].charAt(0)))
                        .append(arr[i].substring(1)).append(" ");
            }
        }
        return sb.toString().trim();
    }

    public void closeDrawer() {
        drawer.closeDrawers();
    }


    private void showmessage(String message) {
        // TODO Auto-generated method stub

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle("Message");

        builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
            }
        });

        android.app.AlertDialog box = builder.create();
        box.show();

    }

    public void showNoCollectionMessage() {
        showmessage("There are no collection dates on this month.");
        // Toast toast = Toast.makeText(this,
        // "There are no collection dates on this month.", Toast.LENGTH_SHORT);
        // toast.show();
    }

    public void displayMessage() {
        showmessage("Reminder set up successfully");
    }

    public void showMessage() {
        Toast toast = Toast.makeText(this,
                "You need to first set the location.", Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("AddressActivity", "onResume: ");

    }

    @Override
    public void onResult(ArrayList<Suburbs.Data> datas) {

    }

    @Override
    public void onError(String error) {
        if (error.equalsIgnoreCase("timeout")) {
            Toast.makeText(this, "Network timeout", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void OnGetStreetList(ArrayList<StreetResponse.Streets> list) {
        getAllStreets(list);
    }

    public void getAllStreets(ArrayList<StreetResponse.Streets> list) {
        dataStreetList = list;
        streetType = new ArrayList<>();
        streetNameList = new ArrayList<>();
        hashMapStreet = new HashMap();
        for (int i = 0; i < dataStreetList.size(); i++) {
            streetNameList.add(dataStreetList.get(i).street);
            streetType.add(dataStreetList.get(i).type);
            //hashMapStreet.put(dataStreetList.get(i).street, dataStreetList.get(i).type);
        }

        for (int i = 0; i < streetNameList.size(); i++) {
            if (!streetNameList.isEmpty()) {
                String address = streetNameList.get(i).toString();
                if (!address.trim().isEmpty()) {
                    String output = address.substring(0, 1).toUpperCase().trim() + address.substring(1).toLowerCase().trim();
                    output = toTitleCase(output);
                    streetNameList.set(i, output);
                    //SubUrbNamesList.add(i,output);
                }
            }
        }

        for (int i = 0; i < streetNameList.size(); i++) {
            if (!streetNameList.isEmpty() && !streetType.isEmpty()) {
                hashMapStreet.put(streetNameList.get(i), streetType.get(i));
            }
        }

        setAutoStreet();
    }

    private void setAutoStreet() {
        adapter = new AutocompleteArrayAdapter(this, streetNameList);
        this.base_street.setAdapter(adapter);
        this.base_street.setThreshold(1);
        this.base_street.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedStreet = (int) hashMapStreet.get(parent.getItemAtPosition(position));
                if (selectedStreet == 1) {
                    et_street_no.setEnabled(true);
                    et_street_no.setHint(getResources().getText(R.string.street_no));
                    et_street_no.setHintTextColor(getResources().getColor(R.color.grey_dark));

                } else {
                    et_street_no.setEnabled(false);
                    et_street_no.setHint(getResources().getText(R.string.no_address));
                    et_street_no.setHintTextColor(getResources().getColor(R.color.white));
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        if (onBackPressedListener != null) {
            onBackPressedListener = null;
        }
        super.onDestroy();
    }

    public void setOnBackPressedListener(OnBackPressedListener mOnBackPressedListener) {
        onBackPressedListener = mOnBackPressedListener;
    }
}



