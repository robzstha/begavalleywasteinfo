package com.wasteinfo.begavalley.bega.Fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.Activities.AddressActivity;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.bega.HelperClasses.GPSTracker;
import com.wasteinfo.begavalley.bega.HelperClasses.KeyboardHelper;
import com.wasteinfo.begavalley.bega.HelperClasses.ReportSpinnerAdapter;
import com.wasteinfo.begavalley.bega.OnBackPressedListener;
import com.wasteinfo.begavalley.bega.RetrofitAPI.MoiraAPI;
import com.wasteinfo.begavalley.bega.RetrofitModel.PostModel;
import com.wasteinfo.begavalley.bega.RetrofitModel.ProblemType;
import com.wasteinfo.begavalley.bega.RetrofitModel.Results;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.db.ProblemTypeDataSource;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.app.Activity.RESULT_OK;


public class ReportProblem extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, OnBackPressedListener {

    private static final String LOGTAG = "WasteInfoVal";
    AddressActivity addressActivity;
    private Context context;
    private Library library;
    File file;
    MultipartBody.Part imagePart;
    private List<String> list;
    private List<String> selectedBinList;
    private List<String> selectedDamagedList;
    private List<String> selectRequiredBinList;
    private List<String> selectReplacementBinList;
    private List<ProblemType> problemTypes;
    private int item_select;
    RequestBody requestFile;
    private int bin_type_select;
    private int required_bintype_select;
    private int replacementBinType_select;
    private int damagedPart_select;
    private String image_url;
    private File image_file;
    private SharedPref shared;
    RequestBody reportRedBinQuantity;
    RequestBody reportOrganicsBinQuantity;
    RequestBody reportRedBinCommercialQuantity;
    RequestBody reportYellowBinQuantity;
    private TextView label_subtitle;
    private TextView label_instruction;
    private TextView f3Agreement;
    private TextView additional_bin_owner;
    private EditText firstname3;
    private EditText lastname3;
    private EditText name;
    private EditText redQuantity;
    private EditText organicsQuantity;
    private EditText yellowQuantity;
    private EditText redCommericalQuantity;
    private EditText f2_report_name;
    private EditText f3_report_name;
    private EditText e_mail;
    private EditText f2_email;
    private EditText f3_email;
    private EditText phone;
    private EditText f2_phone_number;
    private EditText f3_phone_number;
    private EditText f3_report_assesment_number;
    private EditText location_txt;
    private EditText f2_report_location;
    private EditText location3_txt;
    RequestBody reportBinType;
    private EditText notes;
    Uri imageToUploadUri;
    private EditText f2_report_notes;
    private EditText f3_report_notes;
    private CheckBox disclaimer;
    private CheckBox disclaimer2;
    private CheckBox disclaimer3;
    private ImageButton btn_camera;
    private ImageView image;
    CheckBox chkRed;
    CheckBox chkYellow;
    CheckBox chkRedCommercial;
    CheckBox chkOrganics;
    private Spinner type_problem;
    private Spinner select_bin;
    private Spinner replacement_bin;
    private Spinner required_bin;
    private Spinner select_damaged_part;
    private Button btn_send;
    private ImageButton btn_location3;
    private ImageButton btn_location;
    private ImageButton f2_location_btn;
    private OkHttpClient okHttpClient;
    private String url;
    private ProblemTypeDataSource problemTypeDataSource;
    private KeyboardHelper kb_helper;
    LinearLayout layout1;
    LinearLayout layout2;
    LinearLayout layout3;
    ArrayList<String> list_bin = new ArrayList<String>();
    EditText writeAMessage;
    HashMap<String, String> map2;


    public static final String MULTIPART_FORM_DATA = "multipart/form-data";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.addressActivity = (AddressActivity) context;
        this.url = context.getResources().getString(R.string.url);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_report_problem, container, false);

        this.context = container.getContext();
        this.library = new Library(this.context);
        map2 = new HashMap<>();
        kb_helper = new KeyboardHelper();

        ((AddressActivity) getActivity()).setOnBackPressedListener(this);

        shared = new SharedPref(addressActivity);
        this.image_url = "";
        this.chkRed = (CheckBox) rootview.findViewById(R.id.checkRed);
        this.chkYellow = (CheckBox) rootview.findViewById(R.id.checkYellowBin);
        this.chkRedCommercial = (CheckBox) rootview.findViewById(R.id.check_red_commerical);
        this.chkOrganics = (CheckBox) rootview.findViewById(R.id.checkOrganics);

        this.redQuantity = (EditText) rootview.findViewById(R.id.red_quantity);
        this.redCommericalQuantity = (EditText) rootview.findViewById(R.id.red_commerical_quantity);
        this.yellowQuantity = (EditText) rootview.findViewById(R.id.yellow_quantity);
        this.organicsQuantity = (EditText) rootview.findViewById(R.id.organics_quantity);

        this.writeAMessage = (EditText) rootview.findViewById(R.id.othername);

        this.label_subtitle = (TextView) rootview.findViewById(R.id.report_label_subtitle);
        this.f3Agreement = (TextView) rootview.findViewById(R.id.f3Agreement);
        this.additional_bin_owner = (TextView) rootview.findViewById(R.id.additional_bin_owner);
        this.label_instruction = (TextView) rootview.findViewById(R.id.report_label_instruction);
        this.name = (EditText) rootview.findViewById(R.id.report_name);
        this.f2_report_name = (EditText) rootview.findViewById(R.id.f2_report_name);
        this.f3_report_name = (EditText) rootview.findViewById(R.id.f3_report_name);

        this.e_mail = (EditText) rootview.findViewById(R.id.report_email);
        this.f2_email = (EditText) rootview.findViewById(R.id.f2_email);
        this.f3_email = (EditText) rootview.findViewById(R.id.f3_email);
        this.phone = (EditText) rootview.findViewById(R.id.report_phone);
        this.f2_phone_number = (EditText) rootview.findViewById(R.id.f2_phone_number);
        this.f3_phone_number = (EditText) rootview.findViewById(R.id.f3_report_phone);
        this.f3_report_assesment_number = (EditText) rootview.findViewById(R.id.f3_report_assesment_number);
        this.disclaimer = (CheckBox) rootview.findViewById(R.id.report_disclaimer);
        this.disclaimer2 = (CheckBox) rootview.findViewById(R.id.f2_cb_agreement);
        this.disclaimer3 = (CheckBox) rootview.findViewById(R.id.f3_cb_agreement);
        this.btn_camera = (ImageButton) rootview.findViewById(R.id.report_btn_camera);
        this.image = (ImageView) rootview.findViewById(R.id.report_image);
        this.type_problem = (Spinner) rootview.findViewById(R.id.report_type_problems);
        this.select_bin = (Spinner) rootview.findViewById(R.id.sp_which_bin);
        this.select_damaged_part = (Spinner) rootview.findViewById(R.id.sp_damaged_part);
        this.replacement_bin = (Spinner) rootview.findViewById(R.id.sp_request_replacement);
        this.location_txt = (EditText) rootview.findViewById(R.id.report_location);
        this.f2_report_location = (EditText) rootview.findViewById(R.id.f2_report_location);
        this.location3_txt = (EditText) rootview.findViewById(R.id.f3_report_location);
        this.notes = (EditText) rootview.findViewById(R.id.report_notes);
        this.f2_report_notes = (EditText) rootview.findViewById(R.id.f2_report_notes);
        this.f3_report_notes = (EditText) rootview.findViewById(R.id.f3_report_notes);
        this.btn_send = (Button) rootview.findViewById(R.id.report_btn_send);
        this.btn_location = (ImageButton) rootview.findViewById(R.id.location_btn);
        this.f2_location_btn = (ImageButton) rootview.findViewById(R.id.f2_location_btn);
        this.btn_location3 = (ImageButton) rootview.findViewById(R.id.f3_location3_btn);

        //form1
        layout1 = (LinearLayout) rootview.findViewById(R.id.ll_form1);
        //form2
        layout2 = (LinearLayout) rootview.findViewById(R.id.ll_form2);
        //form3
        layout3 = (LinearLayout) rootview.findViewById(R.id.ll_form3);

        addressActivity.tv_title.setText(this.context.getResources().getString(R.string.report_problem));
        addressActivity.toggle.setDrawerIndicatorEnabled(true);
        kb_helper.setupUI(rootview.findViewById(R.id.report_lLactivity), addressActivity);
        kb_helper.setupUI(rootview.findViewById(R.id.LL_main), addressActivity);


        this.problemTypeDataSource = new ProblemTypeDataSource(this.context);
        this.problemTypeDataSource.open();

        this.problemTypes = this.problemTypeDataSource.getAllProblemTypes();

        this.problemTypeDataSource.close();

        this.list = new ArrayList<String>();
        this.selectedBinList = new ArrayList<String>();
        this.selectedDamagedList = new ArrayList<String>();
        this.selectReplacementBinList = new ArrayList<String>();
        this.selectRequiredBinList = new ArrayList<String>();
        //for select Bin Type
        selectedBinList.add("Select Bin");
        selectedBinList.add("140ltr Red Bin");
        selectedBinList.add("240ltr Yellow Bin");
        selectedBinList.add("240ltr Red Bin (commercial only)");
        selectedBinList.add("240ltr Organics Bin");

        this.select_bin.setAdapter(new ReportSpinnerAdapter(this.context, this.selectedBinList));

        select_bin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bin_type_select = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //for selected damaged Part
        selectedDamagedList.add("Select Damaged Bin");
        selectedDamagedList.add("Lid");
        selectedDamagedList.add("Body");
        selectedDamagedList.add("Wheels");
        selectedDamagedList.add("Other");
        selectedDamagedList.add("My bin is not damaged");

        this.select_damaged_part.setAdapter(new ReportSpinnerAdapter(this.context,
                this.selectedDamagedList));

        select_damaged_part.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                damagedPart_select = position;
                if (damagedPart_select == 4) {
                    writeAMessage.setVisibility(View.VISIBLE);
                } else {
                    writeAMessage.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //for replacement bin
        selectReplacementBinList.add("Select Replacement Bin");
        selectReplacementBinList.add("Lost");
        selectReplacementBinList.add("Stolen");
        selectReplacementBinList.add("Destroyed");
        this.replacement_bin.setAdapter(new ReportSpinnerAdapter(this.context,
                this.selectReplacementBinList));

        replacement_bin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                replacementBinType_select = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        try {
            if (!problemTypes.isEmpty()) {
                this.list.add("Choose a Topic");
                // Treatment spinner
                int j = 0;
                for (int i = 0; i < this.problemTypes.size(); i++) {
                    if (this.problemTypes.get(i).getIs_deleted().equalsIgnoreCase("0")) {
                        this.list.add(this.problemTypes.get(i)
                                .getTitle());
                        j++;
                        shared.setKeyValues("Pid" + j, this.problemTypes.get(i).getId());

                    }
                }
                this.type_problem.setAdapter(new ReportSpinnerAdapter(this.context,
                        this.list));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.btn_send.setTypeface(this.library.getDefaultFont("regular"));
        name.setTypeface(this.library.getDefaultFont("regular"));
        f2_report_name.setTypeface(this.library.getDefaultFont("regular"));
        f3_report_name.setTypeface(this.library.getDefaultFont("regular"));
        location_txt.setTypeface(this.library.getDefaultFont("regular"));
        f2_report_location.setTypeface(this.library.getDefaultFont("regular"));
        location3_txt.setTypeface(this.library.getDefaultFont("regular"));
        notes.setTypeface(this.library.getDefaultFont("regular"));
        f2_report_notes.setTypeface(this.library.getDefaultFont("regular"));
        f3_report_notes.setTypeface(this.library.getDefaultFont("regular"));
        e_mail.setTypeface(this.library.getDefaultFont("regular"));
        f2_email.setTypeface(this.library.getDefaultFont("regular"));
        f3_email.setTypeface(this.library.getDefaultFont("regular"));
        f3_report_assesment_number.setTypeface(this.library.getDefaultFont("regular"));
        phone.setTypeface(this.library.getDefaultFont("regular"));
        f2_phone_number.setTypeface(this.library.getDefaultFont("regular"));
        f3_phone_number.setTypeface(this.library.getDefaultFont("regular"));
        label_subtitle.setTypeface(this.library.getDefaultFont("regular"));
        label_instruction.setTypeface(this.library.getDefaultFont("regular"));
        disclaimer.setTypeface(this.library.getDefaultFont("regular"));
        disclaimer2.setTypeface(this.library.getDefaultFont("regular"));
        disclaimer3.setTypeface(this.library.getDefaultFont("regular"));
        chkOrganics.setTypeface(this.library.getDefaultFont("regular"));
        chkYellow.setTypeface(this.library.getDefaultFont("regular"));
        chkRedCommercial.setTypeface(this.library.getDefaultFont("regular"));
        chkRed.setTypeface(this.library.getDefaultFont("regular"));
        writeAMessage.setTypeface(this.library.getDefaultFont("regular"));
        f3Agreement.setTypeface(this.library.getDefaultFont("regular"));
        additional_bin_owner.setTypeface(this.library.getDefaultFont("regular"));

        this.type_problem.setOnItemSelectedListener(this);
        this.btn_camera.setOnClickListener(this);
        this.btn_send.setOnClickListener(this);
        this.btn_location.setOnClickListener(this);
        this.f2_location_btn.setOnClickListener(this);
        this.btn_location3.setOnClickListener(this);
        this.image.setOnClickListener(this);
        checkBoxes();
        return rootview;
    }

    private void checkBoxes() {
        this.chkRed
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton arg0,
                                                 boolean arg1) {
                        // TODO Auto-generated method stub
                        if (arg1) {
                            String redbin = "140ltr Red Bin";
                            map2.put("red_bin", redbin);
                            redQuantity.setText("1");
                        } else {
                            redQuantity.setText("");
                        }
                    }
                });
        this.chkYellow
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton arg0,
                                                 boolean arg1) {
                        // TODO Auto-generated method stub
                        if (arg1) {
                            String yellowBin = "240ltr Yellow Bin";
                            map2.put("yellow_bin", yellowBin);
                            yellowQuantity.setText("1");
                        } else {
                            yellowQuantity.setText("");
                        }
                    }
                });
        this.chkRedCommercial
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton arg0,
                                                 boolean arg1) {
                        // TODO Auto-generated method stub
                        if (arg1) {
                            String red_commercial_bin = "240ltr Red Bin (commercial only)";
                            map2.put("red_commercial_bin", red_commercial_bin);
                            redCommericalQuantity.setText("1");
                        } else {
                            redCommericalQuantity.setText("");
                        }
                    }
                });
        this.chkOrganics
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton arg0,
                                                 boolean arg1) {
                        // TODO Auto-generated method stub
                        if (arg1) {
                            String organics_bin = "240ltr Organics Bin";
                            map2.put("organics_bin", organics_bin);
                            organicsQuantity.setText("1");
                        } else {
                            organicsQuantity.setText("");
                        }
                    }
                });
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == this.btn_location.getId()) {
            permissionCheckLocation();
        }
        if (v.getId() == this.f2_location_btn.getId()) {
            permissionCheckLocation();
        }
        if (v.getId() == this.btn_location3.getId()) {
            permissionCheckLocation();

        }
        if (this.btn_camera.getId() == v.getId()) {
            permissionCheckCamera();
        }

        if (this.btn_send.getId() == v.getId()) {
            InputMethodManager imm = (InputMethodManager) addressActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(addressActivity.getCurrentFocus().getWindowToken(), 0);

            if (this.library.isConnectingToInternet()) {
                if (this.item_select == 3) {
                    if (checkFieldsFor3()) {
                        WebServiceItemNo3();
                    }
                } else if (this.item_select == 5) {
                    if (checkFieldFor5()) {
                        WebServiceItemNo5();
                    }
                } else {
                    if (this.item_select != 0
                            && !this.location_txt.getText().toString().isEmpty()
                            && !this.notes.getText().toString().isEmpty()
                            && !this.name.getText().toString().isEmpty()
                            && !this.e_mail.getText().toString().isEmpty()
                            && (isValid(e_mail.getText().toString().trim()))
                            && !this.phone.getText().toString().isEmpty()
                            ) {
                        webService();

                    } else if (this.item_select == 0) {
                        Toast toast = Toast.makeText(this.context, "Please choose a topic", Toast.LENGTH_LONG);
                        toast.show();

                    } else if (this.location_txt.getText().toString().isEmpty()) {
                        Toast toast = Toast.makeText(this.context, "Please insert location", Toast.LENGTH_LONG);
                        toast.show();

                    } else if (this.notes.getText().toString().isEmpty()) {
                        Toast toast = Toast.makeText(this.context, "Please write a note", Toast.LENGTH_LONG);
                        toast.show();
                    } else if (TextUtils.isEmpty(this.name.getText().toString())) {
                        Toast toast = Toast.makeText(this.context, "Please enter your name", Toast.LENGTH_LONG);
                        toast.show();
                    } else if (TextUtils.isEmpty(this.e_mail.getText().toString())) {
                        Toast toast = Toast.makeText(this.context, "Please enter your email", Toast.LENGTH_LONG);
                        toast.show();
                    } else if (!isValid(this.e_mail.getText().toString())) {
                        Toast toast = Toast.makeText(this.context, "Please enter a valid email", Toast.LENGTH_LONG);
                        toast.show();

                    } else if (TextUtils.isEmpty(this.phone.getText().toString())) {
                        Toast toast = Toast.makeText(this.context, "Please enter your phone number", Toast.LENGTH_LONG);
                        toast.show();
                    }
                }
            } else {
                Toast.makeText(this.context, "No Internet Connection", Toast.LENGTH_LONG).show();
            }

        }

        if (v.getId() == this.image.getId())

        {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(takePictureIntent, 666);
        }

    }

    private boolean checkFieldsFor3() {
        if (this.item_select == 0) {
            displayMessage("Please choose a topic");
            return false;
        }
        if (!isChecked()) {
            displayMessage("Please choose a bin type");
            return false;
        }

        if (this.chkRed.isChecked() && redQuantity.getText().toString().isEmpty()) {
            displayMessage("Insert red bin quantity");
            return false;
        }
        if (this.chkYellow.isChecked() && this.yellowQuantity.getText().toString().isEmpty()) {
            displayMessage("Insert yellow bin quantity");
            return false;

        }
        if (this.chkRedCommercial.isChecked() && this.redCommericalQuantity.getText().toString().isEmpty()) {
            displayMessage("Insert red bin commercial quantity");
            return false;

        }
        if (this.chkOrganics.isChecked() && this.organicsQuantity.getText().toString().isEmpty()) {
            displayMessage("Insert organics bin quantity");
            return false;
        }
        if(this.f3_report_assesment_number.getText().toString().isEmpty()){
            displayMessage("Assessment number is required.");
            return false;
        }

        if (this.location3_txt.getText().toString().isEmpty()) {
            displayMessage("Please insert location");
            return false;
        }

        if (this.f3_report_notes.getText().toString().isEmpty()) {
            displayMessage("Please write a note");
            return false;
        }

        if (this.f3_report_name.getText().toString().isEmpty()) {
            displayMessage("Please enter your name");
            return false;
        }
        if (this.f3_email.getText().toString().isEmpty()) {
            displayMessage("Please enter your email");
            return false;
        }
        if ((!isValid(f3_email.getText().toString().trim()))) {
            displayMessage("Please enter a valid email");
            return false;
        }
        if (this.f3_phone_number.getText().toString().isEmpty()) {
            displayMessage("Please enter your phone number");
            return false;
        }
        return true;
    }

    private boolean checkFieldFor5() {
        if (this.item_select == 0) {
            displayMessage("Please choose a topic");
            return false;
        }
        if (this.bin_type_select == 0) {
            displayMessage("Please choose a bin type");
            return false;
        }
        if (this.damagedPart_select == 0) {
            displayMessage("Please choose damaged type");
            return false;
        }
        if (this.damagedPart_select == 4) {
            if (this.writeAMessage.getText().toString().isEmpty()) {
                displayMessage("Please write name for your damaged bin");
                return false;
            }
        }
        if (this.replacementBinType_select == 0) {
            displayMessage("Please choose a replacement bin");
            return false;
        }
        if (this.f2_report_location.getText().toString().isEmpty()) {
            displayMessage("Please insert location");
            return false;
        }
        if (this.f2_report_notes.getText().toString().isEmpty()) {
            displayMessage("Please write a note");
            return false;
        }
        if (this.f2_report_name.getText().toString().isEmpty()) {
            displayMessage("Please enter your name");
            return false;
        }
        if (this.f2_email.getText().toString().isEmpty()) {
            displayMessage("Please enter your email");
            return false;
        }
        if ((!isValid(f2_email.getText().toString().trim()))) {
            displayMessage("Please enter a valid email");
            return false;
        }
        if (this.f2_phone_number.getText().toString().isEmpty()) {
            displayMessage("Please enter your phone number");
            return false;
        }
        return true;
    }

    private void displayMessage(String s) {
        Toast.makeText(addressActivity, s, Toast.LENGTH_SHORT).show();
    }


    private boolean isChecked() {
        if (this.item_select == 3) {
            if (this.chkOrganics.isChecked() || this.chkRed.isChecked()
                    || this.chkRedCommercial.isChecked() || this.chkYellow.isChecked()) {
                return true;
            } else
                return false;
        } else
            return true;
    }


    private void WebServiceItemNo3() {
        HashMap<String, RequestBody> map = new HashMap<>();

        String type = Integer.toString(shared.getIntValues("Pid" + this.item_select));
        String location = this.location3_txt.getText().toString();
        String notes = this.f3_report_notes.getText().toString();
        String name = this.f3_report_name.getText().toString();
        String email = this.f3_email.getText().toString();
        String phone = this.f3_phone_number.getText().toString();
        String assesmentNumber = this.f3_report_assesment_number.getText().toString();
        String image_url_to_send = this.image_url;
        String redQuantityMaterial;
        String yellowQuantityMaterial;
        String redComeericialQuantityMaterial;
        String organicsQuantityMaterial;
        organicsQuantityMaterial = this.organicsQuantity.getText().toString();
        redComeericialQuantityMaterial = this.redCommericalQuantity.getText().toString();
        redQuantityMaterial = this.redQuantity.getText().toString();
        yellowQuantityMaterial = this.yellowQuantity.getText().toString();

        if (this.item_select == 3) {
            if (this.chkRed.isChecked()) {
                if (redQuantity.getText().equals("1")) {
                    redQuantityMaterial = "1";
                }
                String red = map2.get("red_bin");
                reportBinType = createPartFromString(red);
                reportRedBinQuantity = createPartFromString(redQuantityMaterial);
                map.put("red_weight", reportRedBinQuantity);
                map.put("red_bin", reportBinType);

            }
            if (this.chkYellow.isChecked()) {

                if (yellowQuantity.getText().equals("1")) {
                    yellowQuantityMaterial = "1";
                }
                String yellow = map2.get("yellow_bin");
                reportBinType = createPartFromString(yellow);
                reportYellowBinQuantity = createPartFromString(yellowQuantityMaterial);
                map.put("yellow_weight", reportYellowBinQuantity);
                map.put("yellow_bin", reportBinType);

            }
            if (this.chkRedCommercial.isChecked()) {

                if (redCommericalQuantity.getText().equals("1")) {

                    redComeericialQuantityMaterial = "1";
                }
                String redBinCommercial = map2.get("red_commercial_bin");
                reportBinType = createPartFromString(redBinCommercial);
                reportRedBinCommercialQuantity = createPartFromString(redComeericialQuantityMaterial);
                map.put("red_commercial_bin", reportBinType);
                map.put("red_commercial_weight", reportRedBinCommercialQuantity);

            }
            if (this.chkOrganics.isChecked()) {
                if (organicsQuantity.getText().equals("1")) {
                    organicsQuantityMaterial = "1";
                }
                String organics = map2.get("organics_bin");
                reportBinType = createPartFromString(organics);
                reportOrganicsBinQuantity = createPartFromString(organicsQuantityMaterial);
                map.put("organics_bin", reportBinType);
                map.put("organics_weight", reportOrganicsBinQuantity);
            }
        }


        if (!image_url_to_send.equals("")) {

            file = new File(image_url_to_send);
            requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
            imagePart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        }


        RequestBody reportName = createPartFromString(name);
        RequestBody reportEmail = createPartFromString(email);
        RequestBody reportType = createPartFromString(type);
        RequestBody reportPhone = createPartFromString(phone);
        RequestBody reportLocation = createPartFromString(location);
        RequestBody reportNotes = createPartFromString(notes);
        RequestBody reportAssesmentNo = createPartFromString(assesmentNumber);

        map.put("name", reportName);
        map.put("email", reportEmail);
        map.put("problem_type_id", reportType);
        map.put("phone_no", reportPhone);
        map.put("location", reportLocation);
        map.put("notes", reportNotes);
        map.put("assessment_number", reportAssesmentNo);
        if (imagePart != null) {
            postdata2(imagePart, map);
        } else {
            postdata2(null, map);
        }

        this.btn_camera.setVisibility(View.VISIBLE);
        this.location3_txt.getText().clear();
        this.f3_report_notes.getText().clear();
        this.f3_report_name.getText().clear();
        this.f3_email.getText().clear();
        this.f3_phone_number.getText().clear();
        this.f3_report_assesment_number.getText().clear();
        this.disclaimer3.setChecked(false);
        this.type_problem.setSelection(0);
        this.chkOrganics.setChecked(false);
        this.chkRed.setChecked(false);
        this.chkYellow.setChecked(false);
        this.chkRedCommercial.setChecked(false);
        this.yellowQuantity.getText().clear();
        this.redQuantity.getText().clear();
        this.redCommericalQuantity.getText().clear();
        this.organicsQuantity.getText().clear();
        this.image_url = "";
        this.image.setVisibility(View.GONE);
    }

    private void WebServiceItemNo5() {
        HashMap<String, RequestBody> map = new HashMap<>();

        String type = Integer.toString(shared.getIntValues("Pid" + this.item_select));
        String location = this.f2_report_location.getText().toString();
        String notes = this.f2_report_notes.getText().toString();
        String name = this.f2_report_name.getText().toString();
        String email = this.f2_email.getText().toString();
        String phone = this.f2_phone_number.getText().toString();
        String image_url_toSend = this.image_url;
        String writeAMessage = this.writeAMessage.getText().toString();

        int selectedBinType = this.select_bin.getSelectedItemPosition();
        int selectedDamagedBin = this.select_damaged_part.getSelectedItemPosition();
        int selectedReplacementBin = this.replacement_bin.getSelectedItemPosition();

        String resultSelectedBin = getSelectedItem(selectedBinType);
        String resultDamagedBin = getSelectedDamagedBin(selectedDamagedBin);
        String resultReplacementBin = getSelectedReplacementBin(selectedReplacementBin);

        Log.d("ResultBinType: ", resultSelectedBin);
        Log.d("ResultdamagaedxBin: ", resultDamagedBin);
        Log.d("ResultReplacementBin: ", resultReplacementBin);

        if (!image_url_toSend.equals("")) {
            File file = new File(image_url_toSend);
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
            imagePart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        }

        RequestBody reportName = createPartFromString(name);
        RequestBody reportEmail = createPartFromString(email);
        RequestBody reportType = createPartFromString(type);
        RequestBody reportPhone = createPartFromString(phone);
        RequestBody reportLocation = createPartFromString(location);
        RequestBody reportNotes = createPartFromString(notes);
        RequestBody reportBinType = createPartFromString(resultSelectedBin);
        RequestBody reportDamagedPart = createPartFromString(resultDamagedBin);
        RequestBody reportReplacementBin = createPartFromString(resultReplacementBin);
        RequestBody writeOtherMessage = createPartFromString(writeAMessage);

        map.put("name", reportName);
        map.put("email", reportEmail);
        map.put("problem_type_id", reportType);
        map.put("phone_no", reportPhone);
        map.put("location", reportLocation);
        map.put("notes", reportNotes);
        map.put("bin_type", reportBinType);
        map.put("damaged_part", reportDamagedPart);
        map.put("cause_of_replacement", reportReplacementBin);
        map.put("other_describtion", writeOtherMessage);


        if (imagePart != null) {
            postdata2(imagePart, map);
        } else {
            postdata2(null, map);
        }

        this.btn_camera.setVisibility(View.VISIBLE);
        this.f2_report_location.getText().clear();
        this.f2_report_notes.getText().clear();
        this.f2_report_name.getText().clear();
        this.f2_email.getText().clear();
        this.f2_phone_number.getText().clear();
        this.disclaimer2.setChecked(false);
        this.select_bin.setSelection(0);
        this.replacement_bin.setSelection(0);
        this.select_damaged_part.setSelection(0);
        this.type_problem.setSelection(0);
        this.writeAMessage.getText().clear();
        this.image_url = "";
        this.image.setVisibility(View.GONE);
    }


    private void webService() {
        HashMap<String, RequestBody> map = new HashMap<>();

        String type = Integer.toString(shared.getIntValues("Pid" + this.item_select));
        String location = this.location_txt.getText().toString();
        String notes = this.notes.getText().toString();
        String name = this.name.getText().toString();
        String email = this.e_mail.getText().toString();
        String phone = this.phone.getText().toString();
        String image_urlToSend = this.image_url;

        if (!image_urlToSend.equals("")) {
            file = new File(image_urlToSend);
            requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
            imagePart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        }


        RequestBody reportName = createPartFromString(name);
        RequestBody reportEmail = createPartFromString(email);
        RequestBody reportType = createPartFromString(type);
        RequestBody reportPhone = createPartFromString(phone);
        RequestBody reportLocation = createPartFromString(location);
        RequestBody reportNotes = createPartFromString(notes);

        map.put("name", reportName);
        map.put("email", reportEmail);
        map.put("problem_type_id", reportType);
        map.put("phone_no", reportPhone);
        map.put("location", reportLocation);
        map.put("notes", reportNotes);

        if (imagePart != null) {
            postdata2(imagePart, map);
        } else {
            postdata2(null, map);
        }
        this.btn_camera.setVisibility(View.VISIBLE);
        this.location_txt.getText().clear();
        this.notes.getText().clear();
        this.name.getText().clear();
        this.e_mail.getText().clear();
        this.phone.getText().clear();
        this.disclaimer.setChecked(false);
        this.type_problem.setSelection(0);
        this.image_url = "";
        this.image.setVisibility(View.GONE);
    }

    private void postdata2(MultipartBody.Part image, HashMap<String, RequestBody> map) {
        final ProgressDialog pd = ProgressDialog.show(this.context, "", "Sending report....", true, true);
        final String API = this.url;
        retrofitClient();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MoiraAPI moiraAPI = retrofit.create(MoiraAPI.class);

        Call<PostModel> upload = moiraAPI.sendFeedback(image, map);
        upload.enqueue(new Callback<PostModel>() {
            @Override
            public void onResponse(Call<PostModel> call, Response<PostModel> response) {
                if (response.isSuccessful()) {
                    String code = response.body().success;
                    clearValues();
                    Log.v("Upload", "success");
                    Toast.makeText(context, "Report sent successfully", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();

            }

            @Override
            public void onFailure(Call<PostModel> call, Throwable t) {
//                Log.d("test", "onFailure: " + t.getMessage());
                Toast.makeText(context, "Report sending failed!", Toast.LENGTH_SHORT).show();
//                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }

        });
    }

    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                MediaType.parse(MULTIPART_FORM_DATA), descriptionString);
    }

    private void setMyLocation() {
        if (this.library.isConnectingToInternet()) {
            InputMethodManager imm = (InputMethodManager) addressActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(addressActivity.getCurrentFocus().getWindowToken(), 0);
            Log.d(LOGTAG, "location button clicked!");
            GPSTracker gps;
            final ProgressDialog pd = ProgressDialog.show(this.context, "", "Loading location...", true, true);

            gps = new GPSTracker(addressActivity);
            double lat, lng;
            String latlng = null;

            // Check if GPS enabled
            if (gps.canGetLocation()) {

                lat = gps.getLatitude();
                lng = gps.getLongitude();
                latlng = String.valueOf(lat) + "," + String.valueOf(lng);


                // \n is for new line
//            Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + this.MyLat + "\nLong: " + this.MyLang, Toast.LENGTH_LONG).show();
            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                gps.showSettingsAlert();
            }

            getLocation(pd, latlng);

        } else {
            Toast.makeText(context, "No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    private void getLocation(final ProgressDialog pd, String latlng) {
        final String API = "https://maps.googleapis.com/maps/api/";
        retrofitClient();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MoiraAPI moiraAPI = retrofit.create(MoiraAPI.class);

        Call<Results> listCall = moiraAPI.getCurrentLocation(latlng);
        listCall.enqueue(new Callback<Results>() {
            @Override
            public void onResponse(Call<Results> call, Response<Results> response) {
                if (layout1.getVisibility() == View.VISIBLE) {
                    if (response.body().results.size() > 0) {
                        location_txt.setText(response.body().results.get(0).formattedAddress);
                    } else {
                        Toast.makeText(addressActivity, "Couldn`t get your location", Toast.LENGTH_SHORT).show();
                    }
                }
                if (layout2.getVisibility() == View.VISIBLE) {
                    if (response.body().results.size() > 0) {
                        f2_report_location.setText(response.body().results.get(0).formattedAddress);
                    } else {
                        Toast.makeText(addressActivity, "Couldn`t get your location", Toast.LENGTH_SHORT).show();
                    }
                }
                if (layout3.getVisibility() == View.VISIBLE) {
                    if (response.body().results.size() > 0) {
                        location3_txt.setText(response.body().results.get(0).formattedAddress);
                    } else {
                        Toast.makeText(addressActivity, "Couldn`t get your location", Toast.LENGTH_SHORT).show();
                    }
                }

                pd.dismiss();
            }

            @Override
            public void onFailure(Call<Results> call, Throwable t) {
                Toast.makeText(addressActivity, "Failed", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        });
    }


    public String getSelectedDamagedBin(int position) {
        String damagedBinType = "";
        switch (position) {
            case 1:
                damagedBinType = "LID";
                break;
            case 2:
                damagedBinType = "BODY";
                break;
            case 3:
                damagedBinType = "WHEELS";
                break;
            case 4:
                damagedBinType = "OTHER";
                break;
            case 5:
                damagedBinType = "NOT DAMAGED";
                break;
            default:

                break;
        }
        return damagedBinType.toString();
    }

    public String getSelectedReplacementBin(int position) {
        String replacementBinType = "";
        switch (position) {
            case 1:
                replacementBinType = "LOST";
                break;
            case 2:
                replacementBinType = "STOLEN";
                break;
            case 3:
                replacementBinType = "DESTROYED";
                break;
            default:
                break;
        }
        return replacementBinType.toString();
    }

    public String getSelectedItem(int position) {
        String binType = "";
        switch (position) {
            case 1:
                binType = "140ltr Red Bin";
                break;
            case 2:
                binType = "240ltr Yellow Bin";
                break;
            case 3:
                binType = "240ltr Red Bin (commercial only)";
                break;
            case 4:
                binType = "240ltr Organics Bin";
                break;
            default:

                break;
        }
        return binType.toString();
    }

    private void permissionCheckLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && addressActivity.checkSelfPermission(
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && addressActivity.checkSelfPermission(
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    1200);
            return;
        } else {
            setMyLocation();
        }
    }

    private void setTextLocation(final String location_text) {

        addressActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                location_txt.setText(location_text);
                int position = location_text.length();
                Editable et = location_txt.getText();
                Selection.setSelection(et, position);
            }
        });
    }

    private void retrofitClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(20, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build();
    }

    private void permissionCheckCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && addressActivity.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.CAMERA},
                    1100);

            return;
        } else {
            permissionCheckStorage();
        }
    }

    private void openCameraIntent() {

        InputMethodManager imm = (InputMethodManager) addressActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(addressActivity.getCurrentFocus().getWindowToken(), 0);
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePictureIntent, 666);
    }

    private void permissionCheckStorage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && addressActivity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1300);

            return;
        } else {
            openCameraIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    permissionCheckStorage();
                break;
            case 1200:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    setMyLocation();
                break;
            case 1300:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    openCameraIntent();
                break;

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
        this.item_select = i;
        Log.d(LOGTAG, "Selected Item==" + this.item_select);
        setValues();
        if (i == 3) {
            layout1.setVisibility(View.GONE);
            layout2.setVisibility(View.GONE);
            layout3.setVisibility(View.VISIBLE);
        } else if (i == 5) {
            layout1.setVisibility(View.GONE);
            layout2.setVisibility(View.VISIBLE);
            layout3.setVisibility(View.GONE);
        } else {
            layout1.setVisibility(View.VISIBLE);
            layout2.setVisibility(View.GONE);
            layout3.setVisibility(View.GONE);

        }

    }

    private void clearValues(){
        location3_txt.setText("");
        location_txt.setText("");
        f2_report_location.setText("");
        notes.setText("");
        f2_report_notes.setText("");
        f3_report_notes.setText("");
        name.setText("");
        f3_report_name.setText("");
        f2_report_name.setText("");
        e_mail.setText("");
        f2_email.setText("");
        f3_email.setText("");
        phone.setText("");
        f2_phone_number.setText("");
        f3_phone_number.setText("");
        f3_report_assesment_number.setText("");
        disclaimer.setChecked(false);
    }

    private void setValues() {
        if (layout1.getVisibility() == View.VISIBLE) {
            String loc_txt = location_txt.getText().toString();
            if(!loc_txt.trim().isEmpty()){
                location3_txt.setText(loc_txt);
                f2_report_location.setText(loc_txt);
            }
            String note_txt = notes.getText().toString();
            if(!note_txt.trim().isEmpty()){
                f3_report_notes.setText(note_txt);
                f2_report_notes.setText(note_txt);
            }
            String name_txt = name.getText().toString();
            if(!name_txt.trim().isEmpty()){
                f3_report_name.setText(name_txt);
                f2_report_name.setText(name_txt);
            }
            String email_txt = e_mail.getText().toString();
            if(!email_txt.trim().isEmpty()){
                f3_email.setText(email_txt);
                f2_email.setText(email_txt);
            }
            String phone_txt = phone.getText().toString();
            if(!phone_txt.trim().isEmpty()){
                f2_phone_number.setText(phone_txt);
                f3_phone_number.setText(phone_txt);
            }
            boolean check = disclaimer.isChecked();
            disclaimer2.setChecked(check);
            disclaimer3.setChecked(check);
        } else if (layout2.getVisibility() == View.VISIBLE) {
            String loc_txt = f2_report_location.getText().toString();
            if(!loc_txt.trim().isEmpty()){
                location_txt.setText(loc_txt);
                location3_txt.setText(loc_txt);
            }
            String note_txt = f2_report_notes.getText().toString();
            if(!note_txt.trim().isEmpty()){
                notes.setText(note_txt);
                f3_report_notes.setText(note_txt);
            }
            String name_txt = f2_report_name.getText().toString();
            if(!name_txt.trim().isEmpty()){
                name.setText(name_txt);
                f3_report_name.setText(name_txt);
            }
            String email_txt = f2_email.getText().toString();
            if(!email_txt.trim().isEmpty()){
                e_mail.setText(email_txt);
                f3_email.setText(email_txt);
            }
            String phone_txt = f2_phone_number.getText().toString();
            if(!phone_txt.trim().isEmpty()){
                phone.setText(phone_txt);
                f3_phone_number.setText(phone_txt);
            }
            boolean check = disclaimer2.isChecked();
            disclaimer.setChecked(check);
            disclaimer3.setChecked(check);
        } else if (layout3.getVisibility() == View.VISIBLE) {
            String loc_txt = location3_txt.getText().toString();
            if(!loc_txt.trim().isEmpty()){
                location_txt.setText(loc_txt);
                f2_report_location.setText(loc_txt);
            }
            String note_txt = f3_report_notes.getText().toString();
            if(!note_txt.trim().isEmpty()){
                notes.setText(note_txt);
                f2_report_notes.setText(note_txt);
            }
            String name_txt = f3_report_name.getText().toString();
            if(!name_txt.trim().isEmpty()){
                name.setText(name_txt);
                f2_report_name.setText(name_txt);
            }
            String email_txt = f3_email.getText().toString();
            if(!email_txt.trim().isEmpty()){
                e_mail.setText(email_txt);
                f2_email.setText(email_txt);
            }
            String phone_txt = f3_phone_number.getText().toString();
            if(!phone_txt.trim().isEmpty()){
                phone.setText(phone_txt);
                f2_phone_number.setText(phone_txt);
            }
            boolean check = disclaimer3.isChecked();
            disclaimer2.setChecked(check);
            disclaimer.setChecked(check);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 666 && resultCode == RESULT_OK && data != null) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            this.image.setImageBitmap(photo);

            btn_camera.setVisibility(View.INVISIBLE);
            image.setVisibility(View.VISIBLE);
            Uri tempUri = getImageUri(addressActivity, photo);
            if (tempUri != null) {
                File finalFile = new File(getRealPathFromURI(tempUri));
                Log.d(LOGTAG, "Image Path=" + finalFile.getAbsolutePath());
                this.image_url = finalFile.getAbsolutePath();
            }
        }
    }

    private String getRealPathFromURI(Uri uri) {
        Cursor cursor = addressActivity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private Uri getImageUri(AddressActivity addressActivity, Bitmap photo) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(addressActivity.getContentResolver(), photo, "Title", null);
        return Uri.parse(path);
    }


    public boolean isValid(CharSequence target) {
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void doBack() {
//        int id = AddressActivity.fr_id.get(AddressActivity.fr_id.size() - 2);
//        AddressActivity.fr_id.remove(AddressActivity.fr_id.size() - 1);
//        AddressActivity.bottomNavigation.setCurrentItem(id);
    }
}