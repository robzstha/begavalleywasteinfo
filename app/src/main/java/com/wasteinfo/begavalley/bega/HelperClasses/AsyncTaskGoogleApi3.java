package com.wasteinfo.begavalley.bega.HelperClasses;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.HttpGet;
import com.wasteinfo.begavalley.bega.Activities.DirectionActivity;
import com.wasteinfo.begavalley.bega.Activities.ExternalLinkActivity;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitModel.Page;
import com.wasteinfo.begavalley.bega.RetrofitModel.Step;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.impl.client.BasicResponseHandler;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;



//import com.cyberdesignworks.wasteinfo.moirashire.wasteinfo.MyLocation.LocationResult;
// import com.cyberdesignworks.wasteinfo.moirashire.wasteinfo.model.Step;

public class AsyncTaskGoogleApi3 extends AsyncTask<String,Void,String> {

	private Context context;
	private String url;
	private ProgressDialog pDialog;
	private static final String LOGTAG = "WasteInfoVal";
	private List<Step> step_list;
	private Page page;
	private String source_latlng;
	private String dest_latlng;
	
	public AsyncTaskGoogleApi3(Context context,String url,Page page){
		this.context = context;
		this.url = url;
		this.page=page;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		// Showing progress dialog
		
		pDialog = new ProgressDialog(this.context);
		pDialog.setMessage(Html.fromHtml(this.context.getString(R.string.please_wait)));
				
		pDialog.setCancelable(false);
		pDialog.show();
	}
	
	@Override
	protected String doInBackground(String... params) {
		this.source_latlng=params[0];
		this.dest_latlng=params[1];
		String finalUrl=this.url+"origin="+params[0]+"&destination="+params[1]+"&mode=driving";
//		String finalUrl=this.url+"origin=kathmandu&destination=bhaktapur&mode=driving";
		String response="";
		try
        {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(finalUrl.replaceAll(" ", "%20"));
            ResponseHandler<String> resHandler = new BasicResponseHandler();
            response = httpClient.execute(httpGet, resHandler);
            Log.d(LOGTAG,"Response="+response);
            return response;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return response;  

        }
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		Log.d(LOGTAG,"Result="+result);
		
		try{
			JSONObject jsonObject = new JSONObject(result);
			String status=jsonObject.getString("status");
//			String status="OVER_QUERY_LIMIT";
			if(status.equals("ZERO_RESULTS")){
				Toast toast =Toast.makeText(this.context, "No route could be found between the origin and destination.",Toast.LENGTH_SHORT);
				toast.show();
				SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.context);
				appSharedPrefs.edit().remove("Step_List").commit();
			}else if(status.equals("NOT_FOUND")){
				Toast toast =Toast.makeText(this.context, "At least one of the locations specified in the request's origin, destination, or waypoints could not be geocoded.",Toast.LENGTH_SHORT);
				toast.show();
				SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.context);
				appSharedPrefs.edit().remove("Step_List").commit();
			}else if(status.equals("MAX_WAYPOINTS_EXCEEDED")){
				Toast toast =Toast.makeText(this.context, "Too many waypoints were provided in the request The maximum allowed waypoints is 8, plus the origin, and destination.",Toast.LENGTH_SHORT);
				toast.show();
				SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.context);
				appSharedPrefs.edit().remove("Step_List").commit();
			}else if(status.equals("INVALID_REQUEST")){
				Toast toast =Toast.makeText(this.context, "The provided request was invalid. Common causes of this status include an invalid parameter or parameter value.",Toast.LENGTH_SHORT);
				toast.show();
				SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.context);
				appSharedPrefs.edit().remove("Step_List").commit();
			}else if(status.equals("OVER_QUERY_LIMIT")){
//				Toast toast =Toast.makeText(this.context, "The service has received too many requests from your application within the allowed time period.",Toast.LENGTH_SHORT);
//				toast.show();
				
				SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.context);
				appSharedPrefs.edit().remove("Step_List").commit();
				
				
//				 final ProgressDialog pd = ProgressDialog.show(this.context, "", "Loading...", true);
                 MyLocation.LocationsResult locationResult = new MyLocation.LocationsResult(){
					    @Override
					    public void gotLocation(Location location){
					        //Got the location!
					    if(location != null){
					    	//get latitude and longitude of the location
						     double lng=location.getLongitude();
						     double lat=location.getLatitude();
						     
						     String finalUrl="https://maps.google.com/?saddr="+lat+","+lng+"&daddr="+page.getLatitude()+","+page.getLongitude();
						     //String finalUrl="https://maps.google.com/?saddr="+-27.564333400000000000+","+151.953990900000000000+"&daddr="+page.getLatitude()+","+page.getLongitude();
						     Log.d(LOGTAG,"URL==="+finalUrl);
						     //https://maps.google.com/?saddr=27.7,85.3333&daddr=28.2639,83.9722
						     Intent intent_external_link=new Intent(context,ExternalLinkActivity.class);
						     intent_external_link.putExtra("external_url", finalUrl);
						     context.startActivity(intent_external_link);
						     //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
						     
					    }else{
//					    	showMessage();
					    }
//					    pd.dismiss();
					    }
					};
					MyLocation myLocation = new MyLocation();
					myLocation.getLocation(context, locationResult);
				
			}else if(status.equals("REQUEST_DENIED")){
				Toast toast =Toast.makeText(this.context, "The service denied use of the directions service by your application.",Toast.LENGTH_SHORT);
				toast.show();
				SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.context);
				appSharedPrefs.edit().remove("Step_List").commit();
			}else if(status.equals("UNKNOWN_ERROR")){
				Toast toast =Toast.makeText(this.context, "A directions request could not be processed due to a server error. The request may succeed if you try again.",Toast.LENGTH_SHORT);
				toast.show();
				
				SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.context);
				appSharedPrefs.edit().remove("Step_List").commit();
				
				
				
			}else{
				try{
					JSONObject jObject = new JSONObject(result);
					
					JSONArray routesArray = jObject.optJSONArray("routes");
					
					// Grab the first route
					JSONObject route = routesArray.getJSONObject(0);
					
					//Get the bounds
					JSONObject bounds = route.getJSONObject("bounds");
					
					JSONObject northeastObj= bounds.getJSONObject("northeast");
					
					String northeastLat = northeastObj.getString("lat");
					String northeastLng = northeastObj.getString("lng");
					
					JSONObject southwestObj= bounds.getJSONObject("southwest");
					
					String southwestLat = southwestObj.getString("lat");
					String southwestLng = southwestObj.getString("lng");
					
					SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
			        Editor editor = preferences.edit();
			        editor.putString("NorthEastLat",northeastLat);
			        editor.putString("NorthEastLng",northeastLng);
			        editor.putString("SouthWestLat",southwestLat);
			        editor.putString("SouthWestLng",southwestLng);
			        editor.commit();
					
					// Take all legs from the route
					JSONArray legs = route.getJSONArray("legs");

					// Grab first leg
					JSONObject leg = legs.getJSONObject(0);
					
					JSONArray stepsArray = leg.getJSONArray("steps");
					//use for loop and extract all the step and use delegate to pass to the Direction Activity
					
					int stepsArrayLength = stepsArray.length();
					step_list = new ArrayList<Step>();
					
					for(int i = 0; i < stepsArrayLength; i++){
						/****** Get Object for each JSON node. ***********/
						JSONObject step = stepsArray.getJSONObject(i); 
						
						/******* Fetch node values **********/
						JSONObject distanceObj = step.getJSONObject("distance");
						String distance = distanceObj.getString("text");
						
						JSONObject durationObj = step.getJSONObject("duration");
						String duration = durationObj.getString("text");
						
						String html_instructions = step.getString("html_instructions");
						JSONObject polyline = step.getJSONObject("polyline");
						String points = polyline.getString("points");
						
						JSONObject startLocationObj = step.getJSONObject("start_location");
						String startLat = startLocationObj.getString("lat");
						String startLng = startLocationObj.getString("lng");
						
						JSONObject endLocationObj = step.getJSONObject("end_location");
						String endLat = endLocationObj.getString("lat");
						String endLng = endLocationObj.getString("lng");
						
						Step obj = new Step();
						obj.setDistance(distance);
						obj.setDuration(duration);
						obj.setHtmlInstruction(html_instructions);
						obj.setStartLat(startLat);
						obj.setStartLng(startLng);
						obj.setEndLat(endLat);
						obj.setEndLng(endLng);
						step_list.add(obj);
						
					}
					
					// Dismiss the progress dialog
//					if (pDialog.isShowing())
//						pDialog.dismiss();
					

				}
				catch (JSONException e) {
					e.printStackTrace();
				}
				

				
			
				

				  SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.context);
				  Editor prefsEditor = appSharedPrefs.edit();
				  Gson gson = new Gson();
				  String json = gson.toJson(step_list);
				  prefsEditor.putString("Step_List", json);
				  prefsEditor.commit(); 
				
				

			}
		}catch (JSONException e) {
			e.printStackTrace();
		}
		
		pDialog.dismiss();
		
		Intent intent=new Intent(this.context,DirectionActivity.class);
		intent.putExtra("source_latlng", this.source_latlng);
		intent.putExtra("dest_latlng", this.dest_latlng);
		this.context.startActivity(intent);
	}

}
