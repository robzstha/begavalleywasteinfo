package com.wasteinfo.begavalley.bega.RetrofitModel;

public class Step {

	private int id;
	private String distance;
	private String duration;
	private String html_instruction;
	private String polyline_points;
	private String start_lat;
	private String start_lng;
	private String end_lat;
	private String end_lng;
	
	
	public int getId(){
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getDistance(){
		return distance;
	}
	
	public void setDistance(String distance){
		this.distance=distance;
	}
	
	public String getDuration(){
		return duration;
	}
	
	public void setDuration(String duration){
		this.duration = duration;
	}
	
	public String getHtmlInstruction(){
		return html_instruction;
	}
	
	public void setHtmlInstruction(String html_instruction){
		this.html_instruction = html_instruction;
	}
	
	public String getPolylinePoints(){
		return polyline_points;
	}
	
	public void setPolylinePoints(String polyline_points){
		this.polyline_points = polyline_points;
	}
	
	public String getStartLat(){
		return start_lat;
	}
	
	public void setStartLat(String start_lat){
		this.start_lat = start_lat;
	}
	
	public String getStartLng(){
		return start_lng;
	}
	
	public void setStartLng(String start_lng){
		this.start_lng = start_lng;
	}
	
	public String getEndLat(){
		return end_lat;
	}
	
	public void setEndLat(String end_lat){
		this.end_lat = end_lat;
	}
	
	public String getEndLng(){
		return end_lng;
	}
	
	public void setEndLng(String end_lng){
		this.end_lng = end_lng;
	}
	
}

