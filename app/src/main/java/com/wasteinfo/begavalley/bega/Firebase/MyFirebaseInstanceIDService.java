package com.wasteinfo.begavalley.bega.Firebase;

import android.provider.Settings;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.WebServiceTasks.UpdateDeviceLocation;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    SharedPref pref;
    UpdateDeviceLocation register;
    private String refreshedToken;

    @Override
    public void onTokenRefresh() {

        pref = new SharedPref(getApplicationContext());

        //Getting registration token
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        pref.setKeyValues("firebase_token",refreshedToken);

        if (refreshedToken != null && !refreshedToken.isEmpty()){
            sendRegistrationToServer(refreshedToken);
        } else {
            onTokenRefresh();
        }


        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);

    }

    private void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        //Not required for current project
        String device_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        pref.setKeyValues("device_uuid",device_id);
        register = new UpdateDeviceLocation(getApplicationContext());
        register.UpdateDevice(device_id,token,"2");
    }
}
