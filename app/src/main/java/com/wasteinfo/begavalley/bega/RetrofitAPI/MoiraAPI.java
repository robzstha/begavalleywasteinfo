package com.wasteinfo.begavalley.bega.RetrofitAPI;


import com.wasteinfo.begavalley.bega.RetrofitModel.Location;
import com.wasteinfo.begavalley.bega.RetrofitModel.PostModel;
import com.wasteinfo.begavalley.bega.RetrofitModel.ProblemTypes;
import com.wasteinfo.begavalley.bega.RetrofitModel.Results;
import com.wasteinfo.begavalley.bega.RetrofitModel.Section;
import com.wasteinfo.begavalley.bega.RetrofitModel.StreetResponse;
import com.wasteinfo.begavalley.bega.RetrofitModel.Suburbs;
import com.wasteinfo.begavalley.bega.RetrofitModel.UpdateModel;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

/**
 * Created by Test on 6/24/2016.
 */
public interface MoiraAPI {

    @FormUrlEncoded
    @POST("get_problem_types")
    Call<ProblemTypes> getProblemTypes(@Field("test") String test);

    @FormUrlEncoded
    @POST("location/suburb_search")
    Call<Suburbs> getSuburbs(@Field("suburbs") String suburb);

    @FormUrlEncoded
    @POST("location/search")
    Call<Location> getLocations(@Field("suburb") String suburb,
                                @Field("address") String address,
                                @Field("st_number") String address_no
    );

    @FormUrlEncoded
    @POST("location/auto_suggest")
    Call<StreetResponse> getAutoStreet(@Field("suburb") String suburb);

    @FormUrlEncoded
    @POST("get_updated_data")
    Call<UpdateModel> getUpdatedData(@Field("datetime") String datetime);

    @FormUrlEncoded
    @POST("update_device_location")
    Call<String> updateDevice(@Field("device_uuid") String deviceid,
                              @Field("house_numer") String houseno,
                              @Field("street") String street,
                              @Field("suburb") String suburb,
                              @Field("device_type") String device_type);

    @FormUrlEncoded
    @POST("register_device")
    Call<String> registerDevice(@Field("device_uuid") String deviceid,
                                @Field("device_token") String device_token,
                                @Field("device_type") String device_type);

    @FormUrlEncoded
    @POST("section/get_section_pages_by_section_id")
    Call<Section> getSection(@Field("section_id") int section,
                             @Field("bin_type") int binType);

//    @FormUrlEncoded
//    @POST("feedback")
//    Call<PostModel> sendFeedback(@FieldMap HashMap<String, String> file,
//                                 @FieldMap HashMap<String, String> params);

    @GET("geocode/json")
    Call<Results> getCurrentLocation(@Query("latlng") String latlng);

    @Multipart
    @POST("feedback")
    Call<PostModel> sendFeedback(@Part MultipartBody.Part image,
                                 @PartMap() Map<String, RequestBody> map);

    @Multipart
    @POST("feedback")
    Call<PostModel> sendFeedbackfor5(@Part MultipartBody.Part image,
                                     @Part("name") RequestBody name,
                                     @Part("email") RequestBody email,
                                     @Part("problem_type_id") RequestBody problem_type_id,
                                     @Part("phone_no") RequestBody phone_no,
                                     @Part("location") RequestBody location,
                                     @Part("notes") RequestBody notes,
                                     @Part("bin_type") RequestBody reportBinType,
                                     @Part("damaged_part") RequestBody reportDamagedPart,
                                     @Part("cause_of_replacement") RequestBody reportReplacementBin,
                                     @Part("other_describtion") RequestBody writeOtherMessage
    );

    @Multipart
    @POST("feedback")
    Call<PostModel> sendFeedbackfor3(@Part MultipartBody.Part image,
                                     @Part("name") RequestBody name,
                                     @Part("email") RequestBody email,
                                     @Part("problem_type_id") RequestBody problem_type_id,
                                     @Part("phone_no") RequestBody phone_no,
                                     @Part("location") RequestBody location,
                                     @Part("notes") RequestBody notes,
                                     @Part("bin_type") RequestBody reportBinType,
                                     @Part("assessment_number") RequestBody reportAssesmentNo,
                                     @Part("red_weight") RequestBody reportRedWt,
                                     @Part("yellow_weight") RequestBody reportYellowWt,
                                     @Part("organics_weight") RequestBody reportOrganicsWt,
                                     @Part("red_commercial_weight") RequestBody reportRedCommercialWt
    );

}
