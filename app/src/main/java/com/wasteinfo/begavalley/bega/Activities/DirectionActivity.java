package com.wasteinfo.begavalley.bega.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.HttpGet;
import com.wasteinfo.begavalley.bega.HelperClasses.DirectionArrayAdapter;
import com.wasteinfo.begavalley.bega.HelperClasses.MyLocation;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitModel.Page;
import com.wasteinfo.begavalley.bega.RetrofitModel.Step;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.impl.client.BasicResponseHandler;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

//import org.apache.http.client.ResponseHandler;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.BasicResponseHandler;
//import org.apache.http.impl.client.DefaultHttpClient;

public class DirectionActivity extends AppCompatActivity implements View.OnClickListener {

	private static final String LOGTAG = "WasteInfoVal";
	
	private String source_latlng;
	private String dest_latlng;
	private Page page;
	private ListView list;
	private Button btn_map;
	private Button btn_car;
	private Button btn_bus;
	private Button btn_cycle;
	private Button btn_walk;
	public static ProgressDialog pDialog=null;
	private List<Step> step_list;
	SharedPreferences pref;
	String formatted_source_address;
	String formatted_destination_address;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_direction);
		this.btn_car=(Button) findViewById(R.id.btn_car);
		this.btn_bus=(Button) findViewById(R.id.btn_bus);
		this.btn_cycle=(Button) findViewById(R.id.btn_cycle);
		this.btn_walk=(Button) findViewById(R.id.btn_walk);
		this.btn_map=(Button) findViewById(R.id.btn_map);
		this.list=(ListView) findViewById(R.id.list_direction);
		
		Bundle bundle = getIntent().getExtras();
		source_latlng=bundle.getString("source_latlng");
		dest_latlng=bundle.getString("dest_latlng");
		

		
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		String json=pref.getString("Step_List","");
		
		if(!json.isEmpty()){
			
			Gson gson = new Gson();
			
			java.lang.reflect.Type type = new TypeToken<ArrayList<Step>>(){}.getType();
			ArrayList<Step> stepList = gson.fromJson(json, type);
			
			this.list.setAdapter(new DirectionArrayAdapter(this,stepList));
		}
		
//		AsyncTaskGoogleApi1 runner = new AsyncTaskGoogleApi1(this,"https://maps.googleapis.com/maps/api/geocode/json?",page,list);
//		runner.execute(source_longitude,source_latitude);
		
//		AsyncTaskGoogleApi2 runner = new AsyncTaskGoogleApi2(this,"https://maps.googleapis.com/maps/api/geocode/json?",this.list,page);
//		runner.execute(destination_longitude,destination_latitude);
		
//		this.btn_map.setOnClickListener(this);
//		this.btn_car.setOnClickListener(this);
//		this.btn_cycle.setOnClickListener(this);
//		this.btn_walk.setOnClickListener(this);
//		this.btn_bus.setOnClickListener(this);
	}

//	@Override
//	protected void onStart() {
//		super.onStart();
//		MyAppTracker.getInstance().trackScreenView("DirectionActivity");
//
//	}

	@Override
	protected void onResume() {
		super.onResume();
//		MyAppTracker.getInstance().trackScreenView("DirectionActivity");

	}
//	@Override
//	protected void onStop() {
//		super.onStop();
//		GoogleAnalytics.getInstance(this).reportActivityStop(this);
//	}
	
	@Override
	public void onClick(View v) {
		if(v.getId() == this.btn_map.getId()){
			Intent i = new Intent(this, MapActivity.class);
			startActivity(i);
			overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
		}else if(v.getId() == this.btn_car.getId()){
			
			AsyncTaskGetDirection runner=new AsyncTaskGetDirection(this,"https://maps.googleapis.com/maps/api/directions/json?");
			runner.execute("driving");
		}else if(v.getId() == this.btn_cycle.getId()){
			AsyncTaskGetDirection runner=new AsyncTaskGetDirection(this,"https://maps.googleapis.com/maps/api/directions/json?");
			runner.execute("bicycling");
		}else if(v.getId() == this.btn_walk.getId()){
			AsyncTaskGetDirection runner=new AsyncTaskGetDirection(this,"https://maps.googleapis.com/maps/api/directions/json?");
			runner.execute("walking");
		}else if(v.getId() == this.btn_bus.getId()){
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    fmt.setTimeZone(TimeZone.getTimeZone("GMT"));
		    Date date=null;
		    try{
		    	date = fmt.parse(fmt.format(timestamp));
		    }catch(Exception ex){
		    	ex.printStackTrace();
		    }
		    Date dateAfterAdding30Mins=new Date(date.getTime() + (30 * 60000)); //30 * ONE_MIN_MILLIS
		    Log.d(LOGTAG,"Current time==="+dateAfterAdding30Mins.getTime());
		    AsyncTaskGetDirection runner=new AsyncTaskGetDirection(this,"https://maps.googleapis.com/maps/api/directions/json?");
			runner.execute("transit&departure_time="+dateAfterAdding30Mins.getTime());
			//transit&departure_time=1411473600
		}
		
	}


	private class AsyncTaskGetDirection extends AsyncTask<String,Void,String>{

		private String url;
		private ProgressDialog pDialog;
		private Context context;
		
		public AsyncTaskGetDirection(Context context,String url){
			this.context = context;
			this.url = url;
			
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			
			pDialog = new ProgressDialog(this.context);
			pDialog.setMessage(Html.fromHtml(this.context.getString(R.string.please_wait)));
					
			pDialog.setCancelable(false);
			pDialog.show();
		}
		
		@Override
		protected String doInBackground(String... params) {
			String finalUrl=this.url+"origin="+source_latlng+"&destination="+dest_latlng+"&mode="+params[0];
//			String finalUrl=this.url+"origin=kathmandu&destination=bhaktapur&mode="+params[0];
			String response="";
			try
	        {
	            DefaultHttpClient httpClient = new DefaultHttpClient();
	            HttpGet httpGet = new HttpGet(finalUrl.replaceAll(" ", "%20"));
	            ResponseHandler<String> resHandler = new BasicResponseHandler();
	            response = httpClient.execute(httpGet, resHandler);
	            Log.d(LOGTAG,"Response="+response);
	            return response;
	        }
	        catch(Exception ex)
	        {
	            ex.printStackTrace();
	            return response;  

	        }
			
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.d(LOGTAG,"Result="+result);
			
			try{
				JSONObject jsonObject = new JSONObject(result);
				String status=jsonObject.getString("status");
				
				if(status.equals("ZERO_RESULTS")){
					Toast toast =Toast.makeText(this.context, "No route could be found between the origin and destination.",Toast.LENGTH_SHORT);
					toast.show();
					list.setAdapter(null);
				}else if(status.equals("NOT_FOUND")){
					Toast toast =Toast.makeText(this.context, "At least one of the locations specified in the request's origin, destination, or waypoints could not be geocoded.",Toast.LENGTH_SHORT);
					toast.show();
					list.setAdapter(null);
				}else if(status.equals("MAX_WAYPOINTS_EXCEEDED")){
					Toast toast =Toast.makeText(this.context, "Too many waypointss were provided in the request The maximum allowed waypoints is 8, plus the origin, and destination.",Toast.LENGTH_SHORT);
					toast.show();
					list.setAdapter(null);
				}else if(status.equals("INVALID_REQUEST")){
					Toast toast =Toast.makeText(this.context, "The provided request was invalid. Common causes of this status include an invalid parameter or parameter value.",Toast.LENGTH_SHORT);
					toast.show();
					list.setAdapter(null);
				}else if(status.equals("OVER_QUERY_LIMIT")){
					Toast toast =Toast.makeText(this.context, "The service has received too many requests from your application within the allowed time period.",Toast.LENGTH_SHORT);
					toast.show();
					list.setAdapter(null);
					
					final ProgressDialog pd = ProgressDialog.show(this.context, "", "Loading...", true);
	                 MyLocation.LocationsResult locationResult = new MyLocation.LocationsResult(){
						    @Override
						    public void gotLocation(Location location){
						        //Got the location!
						    if(location != null){
						    	//get latitude and longitude of the location
							     double lng=location.getLongitude();
							     double lat=location.getLatitude();
							     
							     String finalUrl="https://maps.google.com/?saddr="+lat+","+lng+"&daddr="+page.getLatitude()+","+page.getLongitude();
							     //String finalUrl="https://maps.google.com/?saddr="+-27.564333400000000000+","+151.953990900000000000+"&daddr="+page.getLatitude()+","+page.getLongitude();
							     Log.d(LOGTAG,"URL==="+finalUrl);
							     //https://maps.google.com/?saddr=27.7,85.3333&daddr=28.2639,83.9722
							     Intent intent_external_link=new Intent(context, ExternalLinkActivity.class);
							     intent_external_link.putExtra("external_url", finalUrl);
							     context.startActivity(intent_external_link);
							     //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
							     
						    }else{
//						    	showMessage();
						    }
						    pd.dismiss();
						    }
						};
						MyLocation myLocation = new MyLocation();
						myLocation.getLocation(context, locationResult);
					
				}else if(status.equals("REQUEST_DENIED")){
					Toast toast =Toast.makeText(this.context, "The service denied use of the directions service by your application.",Toast.LENGTH_SHORT);
					toast.show();
					list.setAdapter(null);
				}else if(status.equals("UNKNOWN_ERROR")){
					Toast toast =Toast.makeText(this.context, "A directions request could not be processed due to a server error. The request may succeed if you try again.",Toast.LENGTH_SHORT);
					toast.show();
					list.setAdapter(null);
				}else{
					try{
						JSONObject jObject = new JSONObject(result);
						
						JSONArray routesArray = jObject.optJSONArray("routes");
						
						// Grab the first route
						JSONObject route = routesArray.getJSONObject(0);
						
						// Take all legs from the route
						JSONArray legs = route.getJSONArray("legs");

						// Grab first leg
						JSONObject leg = legs.getJSONObject(0);
						
						JSONArray stepsArray = leg.getJSONArray("steps");
						//use for loop and extract all the step and use delegate to pass to the Direction Activity
						
						int stepsArrayLength = stepsArray.length();
						step_list = new ArrayList<Step>();
						
						for(int i = 0; i < stepsArrayLength; i++){
							/****** Get Object for each JSON node. ***********/
							JSONObject step = stepsArray.getJSONObject(i); 
							
							/******* Fetch node values **********/
							JSONObject distanceObj = step.getJSONObject("distance");
							String distance = distanceObj.getString("text");
							
							JSONObject durationObj = step.getJSONObject("duration");
							String duration = durationObj.getString("text");
							
							String html_instructions = step.getString("html_instructions");
							JSONObject polyline = step.getJSONObject("polyline");
							String points = polyline.getString("points");
							
							JSONObject startLocationObj = step.getJSONObject("start_location");
							String startLat = startLocationObj.getString("lat");
							String startLng = startLocationObj.getString("lng");
							
							JSONObject endLocationObj = step.getJSONObject("end_location");
							String endLat = endLocationObj.getString("lat");
							String endLng = endLocationObj.getString("lng");
							
							Step obj = new Step();
							obj.setDistance(distance);
							obj.setDuration(duration);
							obj.setHtmlInstruction(html_instructions);
							obj.setStartLat(startLat);
							obj.setStartLng(startLng);
							obj.setEndLat(endLat);
							obj.setEndLng(endLng);
							step_list.add(obj);
							
						}
						
						

				}catch (JSONException e) {
					e.printStackTrace();
				}
					
					SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.context);
					  Editor prefsEditor = appSharedPrefs.edit();
					  Gson gson = new Gson();
					  String json = gson.toJson(step_list);
					  prefsEditor.putString("Step_List", json);
					  prefsEditor.commit(); 
					
					if(step_list.toArray().length > 0){
						list.invalidate();
						list.setAdapter(new DirectionArrayAdapter(this.context,step_list));
					}

				}
			}catch (JSONException e) {
				e.printStackTrace();
			}
						
			// Dismiss the progress dialog
			if (pDialog.isShowing())
				pDialog.dismiss();
	}
	
	}

}
