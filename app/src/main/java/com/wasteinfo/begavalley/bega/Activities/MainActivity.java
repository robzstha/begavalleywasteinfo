package com.wasteinfo.begavalley.bega.Activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.Alarm.AlarmReceiver;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.bega.HelperClasses.Opener;
import com.wasteinfo.begavalley.bega.RetrofitModel.StreetResponse;
import com.wasteinfo.begavalley.bega.RetrofitModel.Suburbs;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.WebServiceTasks.GetSuburbs;
import com.wasteinfo.begavalley.bega.WebServiceTasks.UpdateDeviceLocation;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/*
* @author: Utsab Malakar
*
* */


public class MainActivity extends Activity implements GetSuburbs.onGetSuburb {

    private SharedPref pref;
    Timer timer;

    TimerTask timerTask;
    protected Opener opener;
    final Handler handler = new Handler();
    private Library library;
    private String device_id;
    private UpdateDeviceLocation updateDeviceLocation;
    String device_uid;
    String firebase_token;
    private GetSuburbs suburbs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        opener = new Opener(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.pref = new SharedPref(this);
        this.library = new Library(this);
        this.device_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
//        pref.setKeyValues("device_uuid",device_id);
        updateDeviceLocation = new UpdateDeviceLocation(this);


        if (this.pref.getStringValues("TimeStamp").isEmpty()) {
            this.pref.setKeyValues("TimeStamp", "");
        }

        Intent intent1 = new Intent(this, AlarmReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(this, 192837, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        // Get the AlarmManager service
        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
//        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 3000, 60000, sender);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60000, sender);

        if (this.library.isConnectingToInternet()) {
            suburbs = new GetSuburbs(this,this);
            suburbs.getSuburb();
        }

//        Intent intent_address = new Intent(this,AddressActivity.class);
//        startActivity(intent_address);
//        startTimer();

    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();
        //initialize the TimerTask's job
//        initializeTimerTask();
        //schedule the timer, after the first 3000ms
        timer.schedule(timerTask, 3000); //

    }


    public void stoptimertask(View v) {

        //stop the timer, if it's not already null

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }


    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        //get the current timeStamp
                        Log.e("Inside Timer", "Timer success");
//                        Intent intent_address = new Intent(MainActivity.this, AddressActivity.class);
//                        startActivity(intent_address);

                    }

                });

            }

        };
    }

    @Override
    public void onResult(ArrayList<Suburbs.Data> datas) {
        opener.SubUrbResult((new Gson()).toJson(datas));

    }
    @Override
    public void onError(String error) {
        if(error.equalsIgnoreCase("timeout")){
            Toast.makeText(this,"Network timeout",Toast.LENGTH_SHORT).show();
            finishAffinity();
        } else {
            Toast.makeText(this,error,Toast.LENGTH_SHORT).show();
            finishAffinity();
        }
    }

    @Override
    public void OnGetStreetList(ArrayList<StreetResponse.Streets> list) {

    }


}
