package com.wasteinfo.begavalley.bega.Interface;

import com.wasteinfo.begavalley.bega.RetrofitModel.StreetResponse;

import java.util.ArrayList;

/**
 * Created by Nikesh on 11/29/2016.
 */

public interface AddressPageInterface {

    void OnGetStreetList(ArrayList<StreetResponse.Streets> list);
}
