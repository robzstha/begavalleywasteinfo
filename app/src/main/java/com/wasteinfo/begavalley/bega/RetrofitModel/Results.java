package com.wasteinfo.begavalley.bega.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Test on 11/4/2016.
 */

public class Results {

    @SerializedName("results")
    @Expose
    public List<CurrentLocation> results = new ArrayList<CurrentLocation>();
    @SerializedName("status")
    @Expose
    public String status;
}
