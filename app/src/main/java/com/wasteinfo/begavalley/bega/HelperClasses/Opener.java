package com.wasteinfo.begavalley.bega.HelperClasses;

import android.app.Activity;
import android.content.Intent;

import com.wasteinfo.begavalley.bega.Activities.AddressActivity;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;


public class Opener {
    Activity activity;
    Intent intent;
    SharedPref pref;

    public Opener(Activity activity) {
        this.activity = activity;
        pref = new SharedPref(activity);
    }


    public void SubUrbResult(String result) {
        intent = new Intent(activity, AddressActivity.class);
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("Suburbs", result);
        activity.startActivity(intent);
    }

}