package com.wasteinfo.begavalley.bega.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.Activities.AddressActivity;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.bega.HelperClasses.AutocompleteArrayAdapter;
import com.wasteinfo.begavalley.bega.HelperClasses.KeyboardHelper;
import com.wasteinfo.begavalley.bega.Interface.AddressPageInterface;
import com.wasteinfo.begavalley.bega.OnBackPressedListener;
import com.wasteinfo.begavalley.bega.RetrofitModel.Page;
import com.wasteinfo.begavalley.bega.RetrofitModel.StreetResponse;
import com.wasteinfo.begavalley.bega.RetrofitModel.Suburbs;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.WebServiceTasks.GetLocation;
import com.wasteinfo.begavalley.bega.WebServiceTasks.GetSuburbs;
import com.wasteinfo.begavalley.bega.db.PageDataSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class AddressFragment extends Fragment implements View.OnFocusChangeListener, GetSuburbs.onGetSuburb, AddressPageInterface, OnBackPressedListener, GetLocation.OnGetLocation {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    ArrayList<StreetResponse.Streets> addressSuggestion;
    private TextView tvRural;
    private Button btn_proceed, btn_email;
    private AutoCompleteTextView tv_suburb;
    private AutoCompleteTextView tv_address;
    private EditText et_street_no;
    private RadioButton radio_guest, radio_res;
    private Library library;
    private Context context;
    private GetSuburbs getAddressAuto;
    AddressPageInterface addressPageInterface;
    private GetLocation getLocation;
    private ArrayList<String> suburb_list;
    String suburbname;
    private ArrayList<String> addressAuto;
    private SharedPref pref;
    private AutocompleteArrayAdapter adapter;
    private TextView tv_label_title;
    private String checkRadio = "2";
    AddressActivity addressActivity;
    ArrayList<String> Suburbname;
    private KeyboardHelper kb_helper;
    List<Suburbs.Data> dataList;
    List<StreetResponse.Streets> dataStreetList;
    ArrayList<String> streetNameList;
    ArrayList<Integer> streetType;
    ArrayList<String> SubUrbNames;
    ArrayList<Integer> SubUrbType;
    private SharedPreferences preferences;
    private GetSuburbs suburbs;
    HashMap hashMapStreet;
    private List<Page> pages;
    private PageDataSource pageDataSource;
    private Page page;
    int selected;
    int selectedStreet;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.addressActivity = (AddressActivity) context;
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_address, container, false);

        this.context = container.getContext();
        ((AddressActivity) getActivity()).setOnBackPressedListener(this);

        kb_helper = new KeyboardHelper();
        kb_helper.setupUI(rootview.findViewById(R.id.add_activity), addressActivity);
        kb_helper.setupUI(rootview.findViewById(R.id.LlayoutId), addressActivity);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        this.pages = new ArrayList<Page>();
        dataList = addressActivity.subUrbData;
        pageDataSource = new PageDataSource(getActivity());
        page = new Page();
        this.tvRural = (TextView) rootview.findViewById(R.id.tv_rural);
        this.btn_proceed = (Button) rootview.findViewById(R.id.address_btn_proceed);
        this.tv_address = (AutoCompleteTextView) rootview.findViewById(R.id.address_value);
        this.tv_suburb = (AutoCompleteTextView) rootview.findViewById(R.id.address_suburb);
        this.et_street_no = (EditText) rootview.findViewById(R.id.et_street_no);
        this.btn_email = (Button) rootview.findViewById(R.id.btn_send_email);
        this.library = new Library(this.context);
        this.pref = new SharedPref(this.context);

        this.getLocation = new GetLocation(this.context,this);
        this.suburb_list = new ArrayList<String>();
        this.addressAuto = new ArrayList<String>();

        addressActivity.tv_title.setText(this.context.getResources().getString(R.string.app_name));
        addressActivity.toggle.setDrawerIndicatorEnabled(false);
        SubUrbNames = new ArrayList<String>();
        SubUrbType = new ArrayList<Integer>();

        final HashMap<String, Integer> hashMap = new HashMap();
        for (int i = 0; i < dataList.size(); i++) {
            SubUrbNames.add(dataList.get(i).suburb);
            SubUrbType.add(dataList.get(i).type);
            hashMap.put(dataList.get(i).suburb, dataList.get(i).type);
        }

        for (int i=0; i<SubUrbNames.size(); i++){
            if(!SubUrbNames.isEmpty()){
                String address = SubUrbNames.get(i).toString();
                if(!address.trim().isEmpty()){
                    String output = address.substring(0, 1).toUpperCase().trim() + address.substring(1).toLowerCase().trim();
                    output = toTitleCase(output);
                    SubUrbNames.set(i,output);
                    //SubUrbNamesList.add(i,output);
                }
            }
        }

        for(int i = 0; i < SubUrbNames.size(); i++){
            if(!SubUrbNames.isEmpty() && !SubUrbType.isEmpty()){
                hashMap.put(SubUrbNames.get(i), SubUrbType.get(i));
            }
        }

        this.adapter = new AutocompleteArrayAdapter(this.context, SubUrbNames);

        this.tv_suburb.setAdapter(adapter);
        this.tv_suburb.setThreshold(1);

        this.btn_proceed.setTypeface(this.library.getDefaultFont("regular"));
        this.tv_address.setTypeface(this.library.getDefaultFont("regular"));
        this.tv_suburb.setTypeface(this.library.getDefaultFont("regular"));
        this.et_street_no.setTypeface(this.library.getDefaultFont("regular"));

        this.tv_suburb.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pref.setKeyValues("selected_suburb", parent.getItemAtPosition(position).toString());

                selected = hashMap.get(parent.getItemAtPosition(position));
                if (selected == 1) {
                    tv_address.setEnabled(true);
                    et_street_no.setEnabled(false);
                    tv_address.setHint(getResources().getText(R.string.address));
                    tv_address.setHintTextColor(getResources().getColor(R.color.grey_dark));
                } else {
                    tv_address.setEnabled(false);
                    tv_address.setHint(getResources().getText(R.string.no_address));
                    tv_address.setHintTextColor(getResources().getColor(R.color.white));
                }
            }
        });

        this.tv_address.setOnFocusChangeListener(this);
        ClickListeners();
        return rootview;

    }


    private void ClickListeners() {
        btn_proceed.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               InputMethodManager imm = (InputMethodManager) addressActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                                               imm.hideSoftInputFromWindow(addressActivity.getCurrentFocus().getWindowToken(), 0);

                                               String suburb_text = tv_suburb.getText().toString().trim();
                                               String address_text = tv_address.getText().toString().trim();
                                               String address_no_text = et_street_no.getText().toString().trim();

                                               if (suburb_text.isEmpty()) {
                                                   Toast toast = Toast.makeText(context,
                                                           "Please search for your address", Toast.LENGTH_LONG);
                                                   toast.show();
                                               } else if (address_text.isEmpty() && selected == 1) {
                                                   Toast toast = Toast.makeText(context,
                                                           "Please enter your street name", Toast.LENGTH_LONG);
                                                   toast.show();
                                               } else if (selectedStreet == 1 && address_no_text.isEmpty()) {
                                                   Toast toast = Toast.makeText(context,
                                                           "Please enter your street number", Toast.LENGTH_LONG);
                                                   toast.show();
                                               } else {
                                                   pref.setKeyValues("user_suburb", suburb_text);
                                                   pref.setKeyValues("user_address", address_text);
                                                   pref.setKeyValues("user_street_number", address_text);
                                                   if (library.isConnectingToInternet()) {
                                                       getLocation.getLocationMethod(suburb_text, address_text, address_no_text);
                                                   }
                                               }
                                           }

                                       }

        );

        tvRural.setOnClickListener(new View.OnClickListener()

                                   {
                                       @Override
                                       public void onClick(View v) {
                                           VisitorFragment visitorFragment = new VisitorFragment();
                                           addressActivity.getSupportFragmentManager().beginTransaction()
                                                   .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                                                   .replace(R.id.contentID, visitorFragment).commit();


                                       }
                                   }


        );
        btn_email.setOnClickListener(new View.OnClickListener()

                                     {
                                         @Override
                                         public void onClick(View v) {

                                             addressActivity.bottomNavigation.setCurrentItem(2);

                                         }
                                     }

        );
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        String suburb_txt = pref.getStringValues("selected_suburb");
        if (!this.tv_address.hasFocus()) {
            tv_address.setHint(getResources().getText(R.string.no_address));
            tv_address.setHintTextColor(getResources().getColor(R.color.white));
        }
        if (!this.et_street_no.hasFocus()) {
            et_street_no.setHint(getResources().getText(R.string.no_address));
            et_street_no.setHintTextColor(getResources().getColor(R.color.white));

        }
        if (et_street_no.hasFocus()) {
            if (selectedStreet == 1) {
                et_street_no.setEnabled(true);
                et_street_no.setHint(getResources().getText(R.string.street_no));
                et_street_no.setHintTextColor(getResources().getColor(R.color.grey_dark));
            }
        }
        if (this.tv_address.hasFocus()) {
            if (selected == 1) {
                tv_address.setEnabled(true);
                et_street_no.setEnabled(false);
                tv_address.setHint(getResources().getText(R.string.address));
                tv_address.setHintTextColor(getResources().getColor(R.color.grey_dark));
            }
            if (this.library.isConnectingToInternet()) {
                String suburb_editTxt = this.tv_suburb.getText().toString();
                if (!suburb_editTxt.isEmpty()) {
                    suburb_editTxt = toTitleCase(suburb_editTxt);
                }

                if (this.SubUrbNames.contains(suburb_editTxt)) {
                    if (this.tv_suburb.getText().length() != 0 && suburb_editTxt.equals(suburb_txt)) {
                        if (this.library.isConnectingToInternet()) {
                            addressSuggestion = new ArrayList<StreetResponse.Streets>();
                            suburbs = new GetSuburbs(getActivity(), this);
                            suburbs.getAutoStreets(suburb_editTxt);
                        } else {
                            Toast toast = Toast.makeText(this.context, "No internet connection!", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                        pref.setKeyValues("selected_suburb", suburb_editTxt);
                        this.tv_address.setText("");
                    }
                } else {
                    adapter = new AutocompleteArrayAdapter(this.context, null);
                    this.tv_address.setAdapter(adapter);
                    pref.setKeyValues("selected_suburb", suburb_editTxt);

                }
            } else {
                Toast toast = Toast.makeText(this.context, "No internet connection!", Toast.LENGTH_SHORT);
                toast.show();
            }
        }

    }

    private void setAutoStreet() {
        adapter = new AutocompleteArrayAdapter(this.context, streetNameList);
        this.tv_address.setAdapter(adapter);
        this.tv_address.setThreshold(1);
        this.tv_address.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedStreet = (int) hashMapStreet.get(parent.getItemAtPosition(position));
                if (selectedStreet == 1) {
                    et_street_no.setEnabled(true);
                    et_street_no.setHint(getResources().getText(R.string.street_no));
                    et_street_no.setHintTextColor(getResources().getColor(R.color.grey_dark));
                } else {
                    et_street_no.setEnabled(false);
                    et_street_no.setHint(getResources().getText(R.string.no_address));
                    et_street_no.setHintTextColor(getResources().getColor(R.color.white));
                }
            }
        });

    }

    public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            if(arr[i].equals("Kb")){
                sb.append("KB").append(" ");
            } else {
                sb.append(Character.toUpperCase(arr[i].charAt(0)))
                        .append(arr[i].substring(1)).append(" ");
            }
        }
        return sb.toString().trim();
    }

    @Override
    public void onResult(ArrayList<Suburbs.Data> datas) {

    }

    @Override
    public void onError(String error) {
        if(error.equalsIgnoreCase("timeout")){
            Toast.makeText(getContext(),"Network timeout",Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(),error,Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void OnGetStreetList(ArrayList<StreetResponse.Streets> list) {
        getAllStreets(list);
    }

    public void getAllStreets(ArrayList<StreetResponse.Streets> list) {
        dataStreetList = list;
        streetType = new ArrayList<>();
        streetNameList = new ArrayList<>();
        hashMapStreet = new HashMap();
        for (int i = 0; i < dataStreetList.size(); i++) {
            streetNameList.add(dataStreetList.get(i).street);
            streetType.add(dataStreetList.get(i).type);
            hashMapStreet.put(dataStreetList.get(i).street, dataStreetList.get(i).type);
        }

        for (int i=0; i<streetNameList.size(); i++){
            if(!streetNameList.isEmpty()){
                String address = streetNameList.get(i).toString();
                if(!address.trim().isEmpty()){
                    String output = address.substring(0, 1).toUpperCase().trim() + address.substring(1).toLowerCase().trim();
                    output = toTitleCase(output);
                    streetNameList.set(i,output);
                    //SubUrbNamesList.add(i,output);
                }
            }
        }

        for(int i = 0; i < streetNameList.size(); i++){
            if(!streetNameList.isEmpty() && !streetType.isEmpty()){
                hashMapStreet.put(streetNameList.get(i), streetType.get(i));
            }
        }

        setAutoStreet();
    }

    @Override
    public void doBack() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to exit?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                addressActivity.finishAffinity();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
