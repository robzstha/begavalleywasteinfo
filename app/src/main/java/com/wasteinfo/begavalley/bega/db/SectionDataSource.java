package com.wasteinfo.begavalley.bega.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.wasteinfo.begavalley.bega.RetrofitModel.Sections;

import java.util.ArrayList;

public class SectionDataSource {
	private static final String LOGTAG = "WasteInfoVal";
	SQLiteOpenHelper dbhelper;
	SQLiteDatabase database;
	
	public SectionDataSource(Context context) {
		dbhelper = new WasteInfoDBOpenHelper(context); 
		
	}
	
	public void open(){
		Log.d(LOGTAG,"Database opened!");
		database = dbhelper.getWritableDatabase();
	}
	
	public void close(){
		Log.d(LOGTAG,"Database closed!");
		dbhelper.close();
	}
	
	public void insertOrUpdateSection(Sections section){
		long id=section.getId();
		Cursor c=database.rawQuery("SELECT * FROM "+WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_SECTION+" WHERE "+WasteInfoDBOpenHelper.COLUMN_SECTION_SECTION_ID+"="+id,null);
		if(c.getCount() == 0){
			//if the row is not present,then insert the row
			ContentValues values = new ContentValues();
			values.put(WasteInfoDBOpenHelper.COLUMN_SECTION_SECTION_ID, id);
			values.put(WasteInfoDBOpenHelper.COLUMN_SECTION_TITLE, section.getTitle());
			values.put(WasteInfoDBOpenHelper.COLUMN_SECTION_PARENT_ID, section.getParentId());
			values.put(WasteInfoDBOpenHelper.COLUMN_SECTION_STATUS,section.getStatus());
			
			
			database.insert(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_SECTION, null, values);
		}else{
			//else update the row
			ContentValues updatedValues =new ContentValues();
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_SECTION_SECTION_ID, id);
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_SECTION_TITLE, section.getTitle());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_SECTION_PARENT_ID, section.getParentId());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_SECTION_STATUS,section.getStatus());
			
			database.update(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_SECTION,updatedValues, WasteInfoDBOpenHelper.COLUMN_SECTION_SECTION_ID+"="+ id ,null );
		}
	}
	
	public Sections getSection(int section_id){
		Cursor c=database.query(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_SECTION,new String[]{
				WasteInfoDBOpenHelper.COLUMN_SECTION_SECTION_ID,
				WasteInfoDBOpenHelper.COLUMN_SECTION_TITLE,
				WasteInfoDBOpenHelper.COLUMN_SECTION_PARENT_ID,
				WasteInfoDBOpenHelper.COLUMN_SECTION_STATUS
		},WasteInfoDBOpenHelper.COLUMN_SECTION_SECTION_ID+"=\'"+section_id+"\'", null,null,null,null);
		if(c.getCount() != 0){
			c.moveToNext();
			Sections section=new Sections();
			section.setId(c.getLong(c.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_SECTION_SECTION_ID)));
			section.setTitle(c.getString(c.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_SECTION_TITLE)));
			section.setParentId(c.getInt(c.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_SECTION_PARENT_ID)));
			section.setStatus(c.getInt(c.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_SECTION_STATUS)));
			c.close();
			return section;
		}
			c.close();
			return null;
		
	}
	
	public ArrayList<Sections> getSections(int parent_id){
		Cursor c_sections=this.database.query(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_SECTION,new String[]{
				WasteInfoDBOpenHelper.COLUMN_SECTION_SECTION_ID,
				WasteInfoDBOpenHelper.COLUMN_SECTION_TITLE,
				WasteInfoDBOpenHelper.COLUMN_SECTION_PARENT_ID,
				WasteInfoDBOpenHelper.COLUMN_SECTION_STATUS
		},WasteInfoDBOpenHelper.COLUMN_SECTION_PARENT_ID+"=\'"+parent_id+"\'",null,null,null,null);
		if(c_sections.getCount() != 0){
			 ArrayList<Sections> sections = new ArrayList<Sections>();
	            while(c_sections.moveToNext()){
	                Sections section = getSection(c_sections.getInt(c_sections.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_SECTION_SECTION_ID)));
	                if(section != null)
	                    sections.add(section);
	            }
	            c_sections.close();
	            return sections;
		}
		c_sections.close();
        return null;
   
	}
}
