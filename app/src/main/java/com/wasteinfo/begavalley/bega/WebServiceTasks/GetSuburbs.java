package com.wasteinfo.begavalley.bega.WebServiceTasks;

import android.content.Context;
import android.util.Log;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.HelperClasses.Opener;
import com.wasteinfo.begavalley.bega.RetrofitAPI.MoiraAPI;
import com.wasteinfo.begavalley.bega.RetrofitModel.StreetResponse;
import com.wasteinfo.begavalley.bega.RetrofitModel.Suburbs;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Test on 6/28/2016.
 */

public class GetSuburbs {
    private String LOGTAG = GetSuburbs.class.getSimpleName();

    private OkHttpClient okHttpClient;

    private Context context;
    private String url;
    SharedPref sharedPref;
    private onGetSuburb listener;
    ArrayList<StreetResponse.Streets> addresslist;

    public GetSuburbs(Context context, onGetSuburb onGetSuburb) {
        this.context = context;
        this.url = context.getResources().getString(R.string.url);
        this.sharedPref = new SharedPref(context);
        this.listener = onGetSuburb;
    }

    public void getSuburb() {
        retrofitClient();
        MoiraAPI moiraAPI = getMoiraAPI();
        Call<Suburbs> suburbs = moiraAPI.getSuburbs("");
        suburbs.enqueue(new Callback<Suburbs>() {
            @Override
            public void onResponse(Call<Suburbs> call, Response<Suburbs> response) {
                Log.d(LOGTAG,"Suburb received!!!");
                if(response.isSuccessful()){
                    if(response.body().getCode().equalsIgnoreCase("0001")){
                        ArrayList<Suburbs.Data> suburbs = new ArrayList<Suburbs.Data>();
                        if(response.body().getSuburbs().size()>0){
                            for (int i = 0; i < response.body().getSuburbs().size(); i++) {
                                suburbs.add(response.body().getSuburbs().get(i));
                            }
                            listener.onResult(suburbs);
                        } else {
                            listener.onError("No suburb available");
                        }
                    } else {
                        listener.onError(response.body().getMsg());
                    }
                } else {
                    Log.d(LOGTAG,"Error: "+response.errorBody().toString());
                    listener.onError(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<Suburbs> call, Throwable t) {
                t.printStackTrace();
                listener.onError(t.getMessage());
            }
        });
    }
    private void retrofitClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(5, TimeUnit.MINUTES)
                .connectTimeout(5, TimeUnit.MINUTES)
                .build();
    }
    private MoiraAPI getMoiraAPI() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(this.url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit.create(MoiraAPI.class);
    }

    public void getAutoStreets(String suburb) {
        retrofitClient();
        MoiraAPI api = getMoiraAPI();
        addresslist = new ArrayList<StreetResponse.Streets>();

        Call<StreetResponse> address = api.getAutoStreet(suburb);
        address.enqueue(new Callback<StreetResponse>() {
            @Override
            public void onResponse(Call<StreetResponse> call, Response<StreetResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getCode().equalsIgnoreCase("0001")){
                        if(response.body().getStreets().size()>0){
                            for (int i = 0; i < response.body().getStreets().size(); i++) {
                                addresslist.add(response.body().getStreets().get(i));
                            }
                            listener.OnGetStreetList(addresslist);
                        } else {
                            listener.onError("No street available");
                        }
                    } else {
                        listener.onError(response.body().getMsg());
                    }
                } else {
                    Log.d(LOGTAG,"Error: "+response.errorBody().toString());
                    listener.onError(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<StreetResponse> call, Throwable t) {
                t.printStackTrace();
                listener.onError(t.getMessage());
            }
        });

    }

    public interface onGetSuburb {
        void onResult(ArrayList<Suburbs.Data> datas);

        void onError(String error);

        void OnGetStreetList(ArrayList<StreetResponse.Streets> list);
    }

}

