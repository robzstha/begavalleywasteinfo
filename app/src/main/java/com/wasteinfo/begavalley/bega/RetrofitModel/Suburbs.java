package com.wasteinfo.begavalley.bega.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Test on 6/24/2016.
 */
public class Suburbs {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("msg")
    @Expose
    private String msg;

    @SerializedName("suburbs")
    @Expose
    private List<Suburbs.Data> suburbs = new ArrayList<Suburbs.Data>();

    public static class Data {
        @SerializedName("suburb")
        @Expose
        public String suburb;

        @SerializedName("type")
        @Expose
        public Integer type;

    }

//    @SerializedName("streets")
//    @Expose
//    private List<String> streets = new ArrayList<String>();




    public String getCode() {
        return code;
    }


    public void setCode(String code) {
        this.code = code;
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Data> getSuburbs() {
        return suburbs;
    }


    public void setSuburbs(List<Data> suburbs) {
        this.suburbs = suburbs;
    }


}
