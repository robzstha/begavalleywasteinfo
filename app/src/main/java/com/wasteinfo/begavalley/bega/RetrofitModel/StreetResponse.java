package com.wasteinfo.begavalley.bega.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikesh on 11/29/2016.
 */

public class StreetResponse {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("msg")
    @Expose
    private String msg;

    @SerializedName("streets")
    @Expose
    private List<Streets> streets = new ArrayList<Streets>();

    public static class Streets {
        @SerializedName("street")
        @Expose
        public String street;

        @SerializedName("type")
        @Expose
        public Integer type;

    }

    public List<Streets> getStreets() {
        return streets;
    }

    public void setStreets(List<Streets> streets) {
        this.streets = streets;
    }

    public String getCode() {
        return code;
    }


    public void setCode(String code) {
        this.code = code;
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
