package com.wasteinfo.begavalley.bega.WebServiceTasks;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.Activities.AddressActivity;
import com.wasteinfo.begavalley.bega.AlarmServices.AlarmNotificationReceiver;
import com.wasteinfo.begavalley.bega.Fragments.CalendarFragment;
import com.wasteinfo.begavalley.bega.RetrofitAPI.MoiraAPI;
import com.wasteinfo.begavalley.bega.RetrofitModel.Location;
import com.wasteinfo.begavalley.bega.RetrofitModel.LocationResult;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.db.LocationDataSource;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by Test on 6/24/2016.
 */
public class GetLocation {

    private LocationDataSource locationDataSource;
    private String url;
    private AddressActivity activity;
    private Context context;
    private SharedPref pref;
    private ArrayList<LocationResult> locresult;
    private String device_uid;
    private OnGetLocation listener;
    private String LOGTAG = GetLocation.class.getSimpleName();

    public GetLocation(Context context,OnGetLocation onGetLocation) {
        this.context = context;
        this.url = context.getResources().getString(R.string.url);
        this.locationDataSource = new LocationDataSource(this.context);
        this.pref = new SharedPref(this.context);
        activity = (AddressActivity) context;
        listener = onGetLocation;
//        this.locationResult = new LocationResult();
    }


    public void getLocationMethod(final String suburb, String street, final String street_no) {
        final ProgressDialog pd = ProgressDialog.show(this.context, "", "Loading....", true, true);
        final String API = this.url;
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(5, TimeUnit.MINUTES)
                .connectTimeout(5, TimeUnit.MINUTES)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MoiraAPI moiraAPI = retrofit.create(MoiraAPI.class);

        Call<Location> suburbsCall = moiraAPI.getLocations(suburb, street, street_no);

        suburbsCall.enqueue(new Callback<Location>() {
            @Override
            public void onResponse(Call<Location> call, Response<Location> response) {
                pd.dismiss();
                if (response.isSuccessful()){
                    if(response.body().getCode().equalsIgnoreCase("0001")){
                        locresult = new ArrayList<LocationResult>();
                        LocationResult locationResult;
                        for (int i = 0; i < response.body().getResults().size(); i++) {
                            locationResult = new LocationResult();
                            locationResult.setItemId(i);
                            locationResult.setUnitNumber(response.body().getResults().get(i).getUnitNumber());
                            locationResult.setHouseNumber(response.body().getResults().get(i).getHouseNumber());
                            locationResult.setStreet(response.body().getResults().get(i).getStreet());
                            locationResult.setSuburb(response.body().getResults().get(i).getSuburb());
                            locationResult.setPostcode(response.body().getResults().get(i).getPostcode());
                            locationResult.setCollectionDay(response.body().getResults().get(i).getCollectionDay());
                            locationResult.setAtlWeek(response.body().getResults().get(i).getAtlWeek());
                            locationResult.setPropertyP(response.body().getResults().get(i).getPropertyP());
                            locationResult.setPfi(response.body().getResults().get(i).getPfi());
                            locationResult.setBinType(response.body().getResults().get(i).getBinType());

                            locationResult.setDateTime(response.body().getResults().get(i).getDateTime());
                            locationResult.setCurrentWeekType(response.body().getResults().get(i).getCurrentWeekType());
                            locationResult.setBinColors(response.body().getResults().get(i).getBinColors());

                            locationResult.setCollectionFrequencies(response.body().getResults().get(i).getCollectionFrequencies());
                            locationResult.setArea(response.body().getResults().get(i).getArea());
                            locationResult.setItemGarbage(response.body().getResults().get(i).getItemGarbage());
                            pref.setKeyValues("item_garbage", response.body().getResults().get(i).getItemGarbage());
                            locationResult.setItemOrganics(response.body().getResults().get(i).getItemOrganics());
                            pref.setKeyValues("item_organic", response.body().getResults().get(i).getItemOrganics());
                            locationResult.setItemRecycling(response.body().getResults().get(i).getItemRecycling());
                            pref.setKeyValues("item_recycling", response.body().getResults().get(i).getItemRecycling());

                            locresult.add(locationResult);
                        }
                        if (locresult.size() == 1) {
                            OpenCalendar(locresult);
                        } else if (locresult.size() == 0) {
                            ShowMessage();
                        } else {
                            OpenCalendar(locresult);
                        }
                    } else {
                        listener.onError(response.body().getMsg());
                    }
                } else {
                    Log.d(LOGTAG,"Error :"+response.errorBody().toString());
                    listener.onError(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<Location> call, Throwable t) {
                pd.dismiss();
                listener.onError(t.getMessage());
            }
        });
    }

    private void showDialogChoice() {
        DialogChoiceAddress dialogObj = new DialogChoiceAddress(this.context, locresult);
        dialogObj.show();
    }

    private void ShowMessage() {

        Toast.makeText(context, "Please enter correct address", Toast.LENGTH_SHORT).show();
    }

    private void OpenCalendar(ArrayList<LocationResult> locatin) {
        LocationResult address = locatin.get(0);
        locationDataSource.open();
        locationDataSource.insertLocation(address);
        locationDataSource.close();
        pref.setKeyValues("item_id", (int) address.getItemId());
        pref.setKeyValues("street_name", address.getStreet());
        pref.setKeyValues("house_number", address.getHouseNumber());
        pref.setKeyValues("collection_day", address.getCollectionDay());
        pref.setKeyValues("collection_week", address.getCollectionDay());
        pref.setKeyValues("collection_frequencies", address.getCollectionFrequencies());
        pref.setKeyValues("week_type_match", address.getAtlWeek());
        pref.setKeyValues("item_garbage", address.getItemGarbage());
        pref.setKeyValues("item_organic", address.getItemOrganics());
        pref.setKeyValues("item_recycling", address.getItemRecycling());
        pref.setKeyValues("bin_colors", address.getBinColors());
        pref.setKeyValues("suburb", address.getSuburb());
        pref.setKeyValues("Area", address.getArea());
        pref.setKeyValues("date_time", address.getDateTime());
        CalendarFragment calendarFragment = new CalendarFragment();
        Bundle args = new Bundle();
        args.putInt("section_id", 3);

        pref.setKeyValues("check_flag_all",0);
        pref.setKeyValues("check_flag_next",0);

        Intent myIntent = new Intent(context, AlarmNotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, myIntent, 0);
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        am.cancel(pendingIntent);

        calendarFragment.setArguments(args);
        activity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentID, calendarFragment).commit();

        device_uid = pref.getStringValues("device_uuid");
        UpdateDeviceLocation updateDeviceLocation = new UpdateDeviceLocation(this.context);
        updateDeviceLocation.UpdateDevice(device_uid, address.getHouseNumber(), address.getStreet(), address.getSuburb(), "2");
    }

    public interface OnGetLocation{
        void onError(String error);
    }
}
