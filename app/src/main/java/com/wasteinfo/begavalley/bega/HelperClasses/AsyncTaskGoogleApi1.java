package com.wasteinfo.begavalley.bega.HelperClasses;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;

import com.loopj.android.http.HttpGet;
import com.wasteinfo.begavalley.bega.Activities.DirectionActivity;
import com.wasteinfo.begavalley.bega.Activities.ExternalLinkActivity;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitModel.Page;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.impl.client.BasicResponseHandler;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class AsyncTaskGoogleApi1 extends AsyncTask<Double, Void, String> {

	private Context context;
	private String url;
	private ProgressDialog pDialog;
	private static final String LOGTAG = "WasteInfoVal";
	private Page page;
	private String destination_longitude;
	private String destination_latitude;
	private double source_latitude;
	private double source_longitude;
	
	public AsyncTaskGoogleApi1(Context context,String url,Page page,double source_latitude,double source_longitude){
		this.context = context;
		this.url = url;
		this.page = page;
		this.destination_longitude=this.page.getLongitude();
		this.destination_latitude=this.page.getLatitude();
		this.source_latitude=source_latitude;
		this.source_longitude=source_longitude;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		// Showing progress dialog
		pDialog = new ProgressDialog(this.context);
		pDialog.setMessage(Html.fromHtml(this.context.getString(R.string.please_wait)));
						
		pDialog.setCancelable(false);
		pDialog.show();
	}
	
	@Override
	protected String doInBackground(Double... params) {
		String finalUrl=this.url+"latlng="+params[1]+","+params[0];
		String response ="";
		try
        {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(finalUrl);
            ResponseHandler<String> resHandler = new BasicResponseHandler();
            response = httpClient.execute(httpGet, resHandler);
            Log.d(LOGTAG,"Response="+response);
            return response;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return response;  

        }
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		
		Log.d(LOGTAG,"Response="+result);
		
		try{
			JSONObject jsonObject = new JSONObject(result);
			String status=jsonObject.getString("status");
//			String status="OVER_QUERY_LIMIT";
			if(status.equals("OVER_QUERY_LIMIT")){
				 String finalUrl="https://maps.google.com/?saddr="+this.source_latitude+","+this.source_longitude+"&daddr="+page.getLatitude()+","+page.getLongitude();
			     Intent intent_external_link=new Intent(context,ExternalLinkActivity.class);
			     intent_external_link.putExtra("external_url", finalUrl);
			     context.startActivity(intent_external_link);
				 
			}else if(status.equals("OK")){
				try{
					JSONObject jObject = new JSONObject(result);
					
					JSONArray jsonMainNode = jObject.optJSONArray("results");
					
					String formatted_address=jsonMainNode.getJSONObject(0).getString("formatted_address").toString();
					Log.d(LOGTAG,"Formatted Address="+formatted_address);
					
					SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this.context); 
					SharedPreferences.Editor editor = pref.edit();
					editor.putString("Source_formatted_address",formatted_address);
					editor.commit();
					
					Intent intent_direction = new Intent(context,DirectionActivity.class);
				    intent_direction.putExtra("destination_longitude",this.destination_longitude);
				    intent_direction.putExtra("destination_latitude",this.destination_latitude);
				    intent_direction.putExtra("Page", (Serializable) page);
				    context.startActivity(intent_direction);
					
				}
				catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}catch (JSONException e) {
			e.printStackTrace();
		}
		
		
		
		// Dismiss the progress dialog
		if (pDialog.isShowing())
			pDialog.dismiss();

	}

}
