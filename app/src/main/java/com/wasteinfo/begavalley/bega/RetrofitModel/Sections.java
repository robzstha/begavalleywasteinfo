package com.wasteinfo.begavalley.bega.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sections {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("section_id")
    @Expose
    private long sectionId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("parent_id")
    @Expose
    private int parentId;
    @SerializedName("status")
    @Expose
    private int status;

    /**
     *
     * @return
     * The sectionId
     */
    public long getSectionId() {
        return sectionId;
    }

    /**
     *
     * @param sectionId
     * The section_id
     */
    public void setSectionId(long sectionId) {
        this.sectionId = sectionId;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The parentId
     */
    public int getParentId() {
        return parentId;
    }

    /**
     *
     * @param parentId
     * The parent_id
     */
    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    /**
     *
     * @return
     * The status
     */
    public int getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
