package com.wasteinfo.begavalley.bega.HelperClasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.R;

import java.util.Calendar;
import java.util.List;

public class CalendarItemArrayAdapter extends ArrayAdapter {

    private Context context;
    private List<CalendarDate> list;
    private int month_id;
    private int day_id;
    Library library;

    @SuppressWarnings("unchecked")
    public CalendarItemArrayAdapter(Context context, List list, int month_id,
                                    int day_id) {

        super(context, R.layout.list_item_calendar_new, list);
        this.context = context;
        this.list = list;
        this.month_id = month_id;
        this.day_id = day_id;
        this.library = new Library(this.context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

		/*LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item_calendar, parent,false);
		FrameLayout layout = (FrameLayout) rowView.findViewById(R.id.calendar_frame_layout);
		TextView name = (TextView) rowView.findViewById(R.id.list_item_calendar_number);
		ImageView symbol_waste = (ImageView) rowView.findViewById(R.id.list_item_calendar_symbol_waste);
		

		name.setText(""+list.get(position).getDate().getDate());
*/
        //Date have event
		/*if(this.list.get(position).getAttachment() != null && this.list.get(position).getAttachment().toArray().length > 0){
			name.setTextColor(this.context.getResources().getColor(R.color.white));
			for(int i = 0; i < this.list.get(position).getAttachment().toArray().length; i++){
                if(this.list.get(position).getAttachment().get(i).getClass().toString().equals(String.class.toString())){
                    
                    if(((String)this.list.get(position).getAttachment().get(i)).equals("organic"))
                    	symbol_waste.setBackgroundColor(Color.parseColor("#006f0a"));
                    if(((String)this.list.get(position).getAttachment().get(i)).equals("recycling"))
                    	symbol_waste.setBackgroundColor(Color.parseColor("#fac200"));
                    if(((String)this.list.get(position).getAttachment().get(i)).equals("paper"))
                    	symbol_waste.setBackgroundColor(Color.parseColor("#0099cc"));
                }
            }
        }*/

        //symbol_event.setVisibility(View.VISIBLE);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item_calendar_new, parent, false);
        RelativeLayout layout = (RelativeLayout) rowView.findViewById(R.id.list_item_calendar_layout);
        TextView name = (TextView) rowView.findViewById(R.id.list_item_calendar_number);
        ImageView symbol_garbage = (ImageView) rowView.findViewById(R.id.list_item_calendar_symbol_garbage);
        ImageView symbol_recycling = (ImageView) rowView.findViewById(R.id.list_item_calendar_symbol_recycling);
        ImageView symbol_organics = (ImageView) rowView.findViewById(R.id.list_item_calendar_symbol_organics);
        ImageView symbol_paper_recycling = (ImageView) rowView.findViewById(R.id.list_item_calendar_symbol_paper_recycling);

        if (list.get(position) != null) {
            name.setText("" + list.get(position).getDate().get(Calendar.DATE));

            //Date have event

            if (this.list.get(position).getAttachment() != null && this.list.get(position).getAttachment().toArray().length > 0) {
                for (int i = 0; i < this.list.get(position).getAttachment().toArray().length; i++) {
                    if (this.list.get(position).getAttachment().get(i).getClass().toString().equals(String.class.toString())) {
                        if (((String) this.list.get(position).getAttachment().get(i)).equals("garbage"))
                            symbol_garbage.setVisibility(View.VISIBLE);
                        if (((String) this.list.get(position).getAttachment().get(i)).equals("organic"))
                            symbol_organics.setVisibility(View.VISIBLE);
                        if (((String) this.list.get(position).getAttachment().get(i)).equals("recycling"))
                            symbol_recycling.setVisibility(View.VISIBLE);
//                        if (((String) this.list.get(position).getAttachment().get(i)).equals("paper"))
//                            symbol_paper_recycling.setVisibility(View.VISIBLE);
                    }
                }
            }


            Calendar c = Calendar.getInstance();
            //Day of today
            if (this.list.get(position).isCurrentDate())
                name.setTextColor(this.context.getResources().getColor(R.color.today));

            //Date is not this month
            if (list.get(position).getDate().get(Calendar.MONTH) != this.month_id) {
                name.setTextColor(this.context.getResources().getColor(R.color.other_month));
            } else {
                //Date is Current
                if (list.get(position).getDate().get(Calendar.DATE) == this.day_id && list.get(position).getDate().get(Calendar.MONTH) == c.get(Calendar.MONTH) && list.get(position).getDate().get(Calendar.YEAR) == c.get(Calendar.YEAR)) {
//                    layout.setBackground(this.context.getResources().getDrawable(R.drawable.background_item_calendar_current));
                    layout.setBackgroundColor(this.context.getResources().getColor(R.color.green_light));
                }

            }

        } else {
            name.setText("");
        }
        //Font treatment
        name.setTypeface(this.library.getDefaultFont("regular"));

        return rowView;
    }

}
