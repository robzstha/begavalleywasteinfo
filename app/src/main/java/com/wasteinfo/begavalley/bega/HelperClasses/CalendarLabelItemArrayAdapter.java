package com.wasteinfo.begavalley.bega.HelperClasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.R;

import java.util.List;

public class CalendarLabelItemArrayAdapter extends ArrayAdapter {

	private Context context;
	private List<String> list;
	private Library library;
	private String day;
	private int year;
	private int month;
	
	@SuppressWarnings("unchecked")
	public CalendarLabelItemArrayAdapter(Context context, List list,String day,int year,int month) {
		super(context, R.layout.list_item_label_calendar,list);
		this.context = context;
        this.list=list;
        this.library=new Library(this.context);
        this.day=day;
        this.year=year;
        this.month=month;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item_label_calendar, parent, false);
        TextView name = (TextView) rowView.findViewById(R.id.list_days_label);
        name.setText(list.get(position));


        
        /*if(this.year == new Date().getYear()){
        	
        	
        	if(this.month >= new Date().getMonth()){
        		 if(this.day.equals(list.get(position))){
               	//Log.d("test","Day is Wednesday");
                	layout.setBackgroundColor(Color.parseColor("#ff0000"));
                	name.setTextColor(Color.parseColor("#FFFFFF"));
                }
        	}
        	
        	
        }
        
        if(this.year > new Date().getYear()){
        	
        	
        	if(this.day.equals(list.get(position))){
               	//Log.d("test","Day is Wednesday");
                	layout.setBackgroundColor(Color.parseColor("#ff0000"));
                	name.setTextColor(Color.parseColor("#FFFFFF"));
                }
        	
        	
        }*/
        
        
        
        //Treatment Font
//        name.setTypeface(this.library.getDefaultFont("regular"));

        
        
        return rowView;
	}
	
}
